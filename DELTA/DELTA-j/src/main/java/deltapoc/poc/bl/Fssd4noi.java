/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                    * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-01-15 07:03:41 PM
**        * FROM NATURAL SUBROUTINE : Fssd4noi
************************************************************
**        * FILE NAME               : Fssd4noi.java
**        * CLASS NAME              : Fssd4noi
**        * INSTANCE NAME           : Fssd4noi
************************************************************
************************************************************************
* THIS SUBROUTINE EXITS SOFRS AND IS ACTIVATED WHEN PF24 IS DEPRESSED**
* MJS 5/21/92
************************************************************************
*
*

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.*;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fssd4noi extends BLNatBase
{
    private static ThreadLocal<List<Fssd4noi>> _instance = new ThreadLocal<List<Fssd4noi>>();
    private static ThreadLocal<Integer> _depth = new ThreadLocal<Integer>();
    private boolean _busy = false;

    public static Fssd4noi getInstance() throws Exception
    {
        if (_instance.get() == null)
        {
            _instance.set(new ArrayList<Fssd4noi>());
            _depth.set(0);
        }

        if (_instance.get().size() == 0)
        {
            _instance.get().add(new Fssd4noi());
        }
        else if (_instance.get().get(_depth.get())._busy) // recursive call
        {
            if (_instance.get().size() - 1 <= _depth.get())
                _instance.get().add(new Fssd4noi());

            _depth.set(_depth.get() + 1);
        }
        return _instance.get().get(_depth.get());
    }

    public static void perform(ProcessState pData, Object... parms) throws Exception
    {
        Fssd4noi instance = null;
        try
        {
            instance = Fssd4noi.getInstance();

            instance._busy = true;

            instance.doPerform(pData, parms);
        }
        finally
        {
            instance._busy = false;
            if (_depth.get() > 0)
                _depth.set(_depth.get() - 1);
        }
    }

    // Data Areas
    private GdaFsgsys gdaFsgsys;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_sofrs_Time;
    private DbsField sofrs_Time_Logon_Id;
    private DbsField sofrs_Time_Logon_Date;
    private DbsField sofrs_Time_Current_Time_Logged_Off;
    private DbsField pnd_Search;

    private DbsGroup pnd_Search__R_Field_1;
    private DbsField pnd_Search_Pnd_Search_Logon;
    private DbsField pnd_Search_Pnd_Search_Date;

    private void initializeFields() throws Exception
    {
        //Data Areas
        gdaFsgsys = GdaFsgsys.getInstance(getCallnatLevel());
        registerRecord(gdaFsgsys);

        // Local Variables
        localVariables = new DbsRecord();

        vw_sofrs_Time = new DataAccessProgramView(new NameInfo("vw_sofrs_Time", "SOFRS-TIME"), "SOFRS_TIME_LOG", "SOFRS_TIME_LOG");
        sofrs_Time_Logon_Id = vw_sofrs_Time.getRecord().newFieldInGroup("sofrs_Time_Logon_Id", "LOGON-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LOGON_ID");
        sofrs_Time_Logon_Date = vw_sofrs_Time.getRecord().newFieldInGroup("sofrs_Time_Logon_Date", "LOGON-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LOGON_DATE");
        sofrs_Time_Current_Time_Logged_Off = vw_sofrs_Time.getRecord().newFieldInGroup("sofrs_Time_Current_Time_Logged_Off", "CURRENT-TIME-LOGGED-OFF", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CURRENT_TIME_LOGGED_OFF");
        registerRecord(vw_sofrs_Time);

        pnd_Search = localVariables.newFieldInRecord("pnd_Search", "#SEARCH", FieldType.STRING, 16);

        pnd_Search__R_Field_1 = localVariables.newGroupInRecord("pnd_Search__R_Field_1", "REDEFINE", pnd_Search);
        pnd_Search_Pnd_Search_Logon = pnd_Search__R_Field_1.newFieldInGroup("pnd_Search_Pnd_Search_Logon", "#SEARCH-LOGON", FieldType.STRING, 8);
        pnd_Search_Pnd_Search_Date = pnd_Search__R_Field_1.newFieldInGroup("pnd_Search_Pnd_Search_Date", "#SEARCH-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_sofrs_Time.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fssd4noi() throws Exception
    {
        super("FSSD4NOI");
        initializeFields();
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        sub_Fssd4noi();                                                                                                                                                   //Natural: FSSD4NOI
    }
    private void sub_Fssd4noi() throws Exception
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  FIND THE TIME LOG RECORD FOR THIS USER AND DATE, AND UPDATE WITH THE
        //*  TIME FOR LOGOFF TIME
        //* ***********************************************************************
        pnd_Search_Pnd_Search_Logon.setValue(Global.getUSER());                                                                                                           //Natural: MOVE *USER TO #SEARCH-LOGON
        pnd_Search_Pnd_Search_Date.setValue(Global.getDATN());                                                                                                            //Natural: MOVE *DATN TO #SEARCH-DATE
        vw_sofrs_Time.startDatabaseFind                                                                                                                                   //Natural: FIND SOFRS-TIME WITH LOGON-ID/DATE = #SEARCH
        (
        "PND_PND_L0350",
        new Wc[] { new Wc("LOGON_ID_DATE", "=", pnd_Search, WcType.WITH) }
        );
        PND_PND_L0350:
        while (condition(vw_sofrs_Time.readNextRow("PND_PND_L0350")))
        {
            vw_sofrs_Time.setIfNotFoundControlFlag(false);
            sofrs_Time_Current_Time_Logged_Off.setValue(Global.getTIMX());                                                                                                //Natural: MOVE *TIMX TO CURRENT-TIME-LOGGED-OFF
            vw_sofrs_Time.updateDBRow("PND_PND_L0350");                                                                                                                   //Natural: UPDATE ( ##L0350. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  (0350)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        DbsUtil.terminate();  if (true) return;                                                                                                                           //Natural: TERMINATE
    }

    //
}
