/* ************************************************************
   **   Modern Systems INC.  COPYRIGHT 2000-2017
   **   eavATP NATURAL/JAVA SaveAs
   **   Product Version: V5.8 - Build 20210115a
   ************************************************************
   ** MOD ID * DESC                 *   DATE
   ************************************************************
   ** INIT   *  INITIAL VERSION     *  2021-01-15 07:03:41 PM
   ************************************************************
   ************************************************************ */
package deltapoc.poc.bl;
import java.util.*;

public class KeyDefinitions {
    public static Map<String,String> getKeyDictionary()
    {
        Map<String, String> keyDictionary = new HashMap<String, String>();

        keyDictionary.put("SOFRS_TIME_LOG_LOGON_ID_DATE", "CHAR(16)");
        keyDictionary.put("FLT_SKED_MASTER_DATE_FLT_NBR_LEG_SEQ_NBR", "BINARY(9)");
        keyDictionary.put("REGION_CODE_MST_STATION_1", "CHAR(3)");
        keyDictionary.put("REGION_CODE_MST_STATION_2", "CHAR(3)");
        keyDictionary.put("FLIGHT_NUMBR_XRF_FLT_NBR_END_DATE_ACT", "BINARY(10)");

        return keyDictionary;
    }
}
