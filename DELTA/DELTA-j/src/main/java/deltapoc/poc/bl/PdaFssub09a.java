/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:39 PM
**        * FROM NATURAL PDA     : FSSUB09A
************************************************************
**        * FILE NAME            : PdaFssub09a.java
**        * CLASS NAME           : PdaFssub09a
**        * INSTANCE NAME        : PdaFssub09a
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.domain.*;

public class PdaFssub09a extends PdaBase
{
    // Properties
    private DbsGroup fssub09a;
    private DbsGroup fssub09a_Pnd_Inputs;
    private DbsField fssub09a_Flight_Number;
    private DbsGroup fssub09a_Flight_NumberRedef1;
    private DbsField fssub09a_Flight_Number_Num;
    private DbsField fssub09a_Pnd_Flight_Date;
    private DbsGroup fssub09a_Pnd_Outputs;
    private DbsField fssub09a_Flight_Type;

    public DbsGroup getFssub09a() { return fssub09a; }

    public DbsGroup getFssub09a_Pnd_Inputs() { return fssub09a_Pnd_Inputs; }

    public DbsField getFssub09a_Flight_Number() { return fssub09a_Flight_Number; }

    public DbsGroup getFssub09a_Flight_NumberRedef1() { return fssub09a_Flight_NumberRedef1; }

    public DbsField getFssub09a_Flight_Number_Num() { return fssub09a_Flight_Number_Num; }

    public DbsField getFssub09a_Pnd_Flight_Date() { return fssub09a_Pnd_Flight_Date; }

    public DbsGroup getFssub09a_Pnd_Outputs() { return fssub09a_Pnd_Outputs; }

    public DbsField getFssub09a_Flight_Type() { return fssub09a_Flight_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        fssub09a = dbsRecord.newGroupInRecord("fssub09a", "FSSUB09A");
        fssub09a.setParameterOption(ParameterOption.ByReference);
        fssub09a_Pnd_Inputs = fssub09a.newGroupInGroup("fssub09a_Pnd_Inputs", "#INPUTS");
        fssub09a_Flight_Number = fssub09a_Pnd_Inputs.newFieldInGroup("fssub09a_Flight_Number", "FLIGHT-NUMBER", FieldType.STRING, 5);
        fssub09a_Flight_NumberRedef1 = fssub09a_Pnd_Inputs.newGroupInGroup("fssub09a_Flight_NumberRedef1", "Redefines", fssub09a_Flight_Number);
        fssub09a_Flight_Number_Num = fssub09a_Flight_NumberRedef1.newFieldInGroup("fssub09a_Flight_Number_Num", "FLIGHT-NUMBER-NUM", FieldType.NUMERIC, 
            5);
        fssub09a_Pnd_Flight_Date = fssub09a_Pnd_Inputs.newFieldInGroup("fssub09a_Pnd_Flight_Date", "#FLIGHT-DATE", FieldType.STRING, 8);
        fssub09a_Pnd_Outputs = fssub09a.newGroupInGroup("fssub09a_Pnd_Outputs", "#OUTPUTS");
        fssub09a_Flight_Type = fssub09a_Pnd_Outputs.newFieldInGroup("fssub09a_Flight_Type", "FLIGHT-TYPE", FieldType.STRING, 5);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFssub09a(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

