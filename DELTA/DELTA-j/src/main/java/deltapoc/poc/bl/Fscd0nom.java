/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-01-15 07:03:39 PM
**        *   FROM NATURAL MAP   :  Fscd0nom
************************************************************
**        * FILE NAME               : Fscd0nom.java
**        * CLASS NAME              : Fscd0nom
**        * INSTANCE NAME           : Fscd0nom
************************************************************
* MAP2: PROTOTYPE              * INPUT USING MAP 'XXXXXXXX'   *     #COMMAND #DISPLAY-HALF1(*,*) #PROGRAM
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fscd0nom extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Command;
    private DbsField pnd_Display_Half1;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Command = parameters.newFieldInRecord("pnd_Command", "#COMMAND", FieldType.STRING, 5);
        pnd_Display_Half1 = parameters.newFieldArrayInRecord("pnd_Display_Half1", "#DISPLAY-HALF1", FieldType.STRING, 38, new DbsArrayController(1, 15, 
            1, 2));
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Fscd0nom() throws Exception
    {
        super("Fscd0nom");
        initializeFields();
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=023 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fscd0nom", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("astUSER", Global.getUSER(), true, 1, 2, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 16, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "S O F R S", "", 1, 36, 9);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 58, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 1, 72, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "CONNECTION CARRIER LIST", "TURQUOISE", 3, 29, 23);
            uiForm.setUiLabel("label_3", "STRT", "", 5, 29, 4);
            uiForm.setUiLabel("label_4", "END", "", 5, 35, 3);
            uiForm.setUiLabel("label_5", "STRT", "", 5, 69, 4);
            uiForm.setUiLabel("label_6", "END", "", 5, 75, 3);
            uiForm.setUiLabel("label_7", "AC AIRLINE NAME", "", 6, 2, 15);
            uiForm.setUiLabel("label_8", "DATE", "", 6, 29, 4);
            uiForm.setUiLabel("label_9", "DATE", "", 6, 35, 4);
            uiForm.setUiLabel("label_10", "AC AIRLINE NAME", "", 6, 42, 15);
            uiForm.setUiLabel("label_11", "DATE", "", 6, 69, 4);
            uiForm.setUiLabel("label_12", "DATE", "", 6, 75, 4);
            uiForm.setUiLabel("label_13", "--", "", 7, 2, 2);
            uiForm.setUiLabel("label_14", "-----------------------", "", 7, 5, 23);
            uiForm.setUiLabel("label_15", "----- -----", "", 7, 29, 11);
            uiForm.setUiLabel("label_16", "--", "", 7, 42, 2);
            uiForm.setUiLabel("label_17", "-----------------------", "", 7, 45, 23);
            uiForm.setUiLabel("label_18", "----- -----", "", 7, 69, 11);
            uiForm.setUiControl("pnd_Display_Half1_1_1", pnd_Display_Half1.getValue(1,1), true, 8, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_1_2", pnd_Display_Half1.getValue(1,2), true, 8, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_2_1", pnd_Display_Half1.getValue(2,1), true, 9, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_2_2", pnd_Display_Half1.getValue(2,2), true, 9, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_3_1", pnd_Display_Half1.getValue(3,1), true, 10, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_3_2", pnd_Display_Half1.getValue(3,2), true, 10, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_4_1", pnd_Display_Half1.getValue(4,1), true, 11, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_4_2", pnd_Display_Half1.getValue(4,2), true, 11, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_5_1", pnd_Display_Half1.getValue(5,1), true, 12, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_5_2", pnd_Display_Half1.getValue(5,2), true, 12, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_6_1", pnd_Display_Half1.getValue(6,1), true, 13, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_6_2", pnd_Display_Half1.getValue(6,2), true, 13, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_7_1", pnd_Display_Half1.getValue(7,1), true, 14, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_7_2", pnd_Display_Half1.getValue(7,2), true, 14, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_8_1", pnd_Display_Half1.getValue(8,1), true, 15, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_8_2", pnd_Display_Half1.getValue(8,2), true, 15, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_9_1", pnd_Display_Half1.getValue(9,1), true, 16, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_9_2", pnd_Display_Half1.getValue(9,2), true, 16, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_10_1", pnd_Display_Half1.getValue(10,1), true, 17, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_10_2", pnd_Display_Half1.getValue(10,2), true, 17, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_11_1", pnd_Display_Half1.getValue(11,1), true, 18, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_11_2", pnd_Display_Half1.getValue(11,2), true, 18, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_12_1", pnd_Display_Half1.getValue(12,1), true, 19, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_12_2", pnd_Display_Half1.getValue(12,2), true, 19, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_13_1", pnd_Display_Half1.getValue(13,1), true, 20, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_13_2", pnd_Display_Half1.getValue(13,2), true, 20, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_14_1", pnd_Display_Half1.getValue(14,1), true, 21, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_14_2", pnd_Display_Half1.getValue(14,2), true, 21, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_15_1", pnd_Display_Half1.getValue(15,1), true, 22, 2, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Display_Half1_15_2", pnd_Display_Half1.getValue(15,2), true, 22, 42, 38, "", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_19", "COMMAND===>", "", 23, 2, 11);
            uiForm.setUiControl("pnd_Command", pnd_Command, false, 23, 15, 5, "", true, false, null, null, "AD=D?MFHT' '~TG=", ' ', "FSSD2NOI");
            uiForm.setUiLabel("label_20", "PF1=HELP", "", 23, 24, 8);
            uiForm.setUiLabel("label_21", "PF2=PREVIOUS SCREEN", "", 23, 36, 19);
            uiForm.setUiLabel("label_22", "PF3=EXIT", "", 23, 59, 8);
            uiForm.setUiLabel("label_23", "PF8=PRINT", "", 23, 71, 9);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
