/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-01-15 07:03:39 PM
**        *   FROM NATURAL MAP   :  Fssd3nom
************************************************************
**        * FILE NAME               : Fssd3nom.java
**        * CLASS NAME              : Fssd3nom
**        * INSTANCE NAME           : Fssd3nom
************************************************************
************************************************************************   * CONVERTED TO STRUCTURED MODE ON 1/8/2021 AT 11:20:44   ************************************************************************ 
    * MAP2: PROTOTYPE   * INPUT USING MAP 'XXXXXXXX'   *     #PRINTER-ADDR #PROGRAM
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fssd3nom extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Printer_Addr;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Printer_Addr = parameters.newFieldInRecord("pnd_Printer_Addr", "#PRINTER-ADDR", FieldType.NUMERIC, 4);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Fssd3nom() throws Exception
    {
        super("Fssd3nom");
        initializeFields();
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=023 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fssd3nom", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("astUSER", Global.getUSER(), true, 1, 2, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 16, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "S O F R S A", "", 1, 36, 11);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 58, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 1, 72, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "PRINTER REQUEST SCREEN", "", 3, 29, 22);
            uiForm.setUiLabel("label_3", "PLEASE ENTER PRINTER ID :", "", 8, 25, 25);
            uiForm.setUiControl("pnd_Printer_Addr", pnd_Printer_Addr, false, 8, 51, 4, "", true, false, null, "0123456789+-, ", "AD=ILMFHT'_'~TG=", '_');
            uiForm.setUiLabel("label_4", "(OR LEAVE BLANK TO SEND REPORT TO TRMS)", "", 10, 24, 39);
            uiForm.setUiLabel("label_5", "PF3=EXIT", "", 23, 37, 8);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
