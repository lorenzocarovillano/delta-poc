package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.io.*;
import ateras.framework.io.common.Settings;
import ateras.framework.ui.*;
import ateras.framework.system.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.io.File;


public class StartMenu extends BLNatBase
{
    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Action_Id;
    private DbsField pnd_Start_Program;
    private DbsField pnd_Name;
    private DbsField pnd_Entry_Point;
    private DbsField pnd_Entry_Points;
    
    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
    	localVariables = new DbsRecord();
    	pnd_Action_Id = localVariables.newFieldInRecord("pnd_Action_Id", "#ACTION-ID", FieldType.INTEGER, 2);
    	pnd_Start_Program = localVariables.newFieldInRecord("pnd_Start_Program", "#START-PROGRAM", FieldType.STRING, 8);
    	pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 60);
    	pnd_Entry_Point = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 2);
    	pnd_Entry_Points = localVariables.newFieldArrayInRecord("pnd_Entry_Points", "#ENTRY-POINTS", FieldType.STRING, 8, new DbsArrayController(1, 20));
    	localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }
    
    @Override
    public void initializeValues() throws Exception
    {
    	localVariables.reset();
    }
    
    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor
    public StartMenu() throws Exception
    {
        super("StartMenu");
        initializeFields();
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    
    private void runMain() throws Exception
    {
        setLocalMethod("StartMenu|Main");
        getReports().atTopOfPage(atTopEventRpt0, 0);
        getReports().atEndOfPage(atEndEventRpt0, 0);
        setupReports();
        while(true)
        {
            String path = Settings.getBlDirectory() + "EntryPoints.XML";
            SimpleLogging.logMandatoryMessage("Looking for EntryPoints.XML file in: " + path);
            if ((path == null || path.isEmpty()) || !new File(path).exists())
            {
                path = System.getProperty("user.dir") + "\\EntryPoints.XML";
                SimpleLogging.logMandatoryMessage("Looking for EntryPoints.XML file in: " + path);
	            if (!new File(path).exists())
	            {
	            	path = this.getClass().getProtectionDomain().getCodeSource().getLocation() + "EntryPoints.XML";
	            	SimpleLogging.logMandatoryMessage("Looking for EntryPoints.XML file in: " + path);
	            	if (!new File(path).exists())
	            	{
	            		path = new File(".").getCanonicalPath() + "\\EntryPoints.XML";
	            		SimpleLogging.logMandatoryMessage("Looking for EntryPoints.XML file in: " + path);
	            		if (!new File(path).exists())
	            		{
	            			SimpleLogging.logMandatoryMessage("Could not find EntryPoints xml file. File path: '" + path + "'");
	            			return;
	            		}
	            	}
	            }
            }
            
	        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
	        
	        DefaultHandler handler = new DefaultHandler()
			{
	        	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
	        	{
	        		try
	        		{
    	        		if (qName.equals("EntryPoint"))
    	        		{
    	        			pnd_Action_Id.nadd(1);
    	        			pnd_Start_Program.setValue(attributes.getValue("StartProgram"));
    	        			pnd_Name.setValue(attributes.getValue("Name"));
    	        			pnd_Entry_Points.getValue(pnd_Action_Id).setValue(pnd_Start_Program);
    	        			
    	        			getReports().write(0, ReportOption.NOTITLE, ReportOption.NOHDR, "   ", pnd_Action_Id, new FieldAttributes("AD=I"), ": ", pnd_Start_Program, new FieldAttributes("CD=YE"), " ", pnd_Name, new FieldAttributes("CD=GR"));
    	        		}
	        		}
	        		catch(Exception e)
	        		{
	        			try 
	        			{
							displayError(e);
						} 
	        			catch (Exception e1) 
	        			{
							e1.printStackTrace();
						}
	        		}
	        	}
	        	
	        	public void endElement(String uri, String localName, String qName) throws SAXException
	        	{
	        		
	        	}
			};
			
	        parser.parse(path, handler);
	        
	        while(true)
	        {
	        	getReports().write(0, ReportOption.NOTITLE, ReportOption.NOHDR, " ");
	        	if (Global.isEscapeBottom()) break;
	        }
	        
	        String startProgram = pnd_Entry_Points.getValue(pnd_Action_Id).getText();
	        Global.setFetchProgram(DbsUtil.getBlType(startProgram));
        
	        break;
        }
    }
    
    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            getReports().write(0, ReportOption.NOTITLE, ReportOption.NOHDR, " ");
            getReports().write(0, ReportOption.NOTITLE, ReportOption.NOHDR, " ");
            getReports().write(0, ReportOption.NOTITLE, ReportOption.NOHDR, " ");
        }
        catch (Exception ex)
        {
            try
            {
				displayError(ex);
			} 
            catch (Exception e1) 
            {
				e1.printStackTrace();
			}
        }
    };
    
    public AtItemEventHandler atEndEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                	DbsUtil.invokeInput(setInputStatus(INPUT_1), this, "Entry point: ", pnd_Entry_Point);
		        	
			        String entryPoint = pnd_Entry_Point.getText().trim();
			        if (entryPoint.length() == 0)
			        {
			        	Map.reinput(new ReinputWithText("Please enter entry point"));			        	
			        }
			        else if (entryPoint.chars().allMatch(Character::isDigit))
			        {
			        	int action = Integer.parseInt(entryPoint);
			        	if (action > 0 && action <= pnd_Action_Id.getInt())
			        	{
				        	pnd_Action_Id.setValue(entryPoint);
				        	Global.setEscape(true);
				        	Global.setEscapeCode(EscapeType.Bottom);			        	
			        	}
			        	else
			        		Map.reinput(new ReinputWithText("Invalid entry point"));
			        }
			        else
			        	Map.reinput(new ReinputWithText("Invalid entry point"));
                }
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                ex.printStackTrace();
        }
    };
    
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=22");
    }
}
