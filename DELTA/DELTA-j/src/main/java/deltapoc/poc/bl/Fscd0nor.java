/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:39 PM
**        * FROM NATURAL PROGRAM : Fscd0nor
************************************************************
**        * FILE NAME            : Fscd0nor.java
**        * CLASS NAME           : Fscd0nor
**        * INSTANCE NAME        : Fscd0nor
************************************************************
*****************************************************************
** THIS IS THE PROGRAM THAT DISPLAYS THE CONNECTION CARRIERS
** THIS PROGRAM IS ACTIVATED BY FSTBLNOI (SOFRS TABLE DISPLAY MENU)
**
** DIANE KENNEDY
**
** 3/07/07 TML - EXPANDED MAP TO 28 AIRLINES.
*****************************************************************

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fscd0nor extends BLNatBase
{
    // Data Areas
    private GdaFsgsys gdaFsgsys;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnt;
    private DbsField pnd_Command_Compress;

    private DbsGroup pnd_Command_Compress__R_Field_1;
    private DbsField pnd_Command_Compress_Pnd_C1;
    private DbsField pnd_Command_Compress_Pnd_C2;
    private DbsField pnd_Command_Compress_Pnd_C3;
    private DbsField pnd_Command_Compress_Pnd_C4;
    private DbsField pnd_Command_Compress_Pnd_C5;
    private DbsField pnd_Display_Half1;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Rec_Found;
    private DbsField pnd_Wk_Line;

    private DbsGroup pnd_Wk_Line__R_Field_2;
    private DbsField pnd_Wk_Line_Pnd_Aircd;
    private DbsField pnd_Wk_Line_Pnd_Fill;
    private DbsField pnd_Wk_Line_Pnd_Airline_Name;
    private DbsField pnd_Wk_Line_Pnd_Fill1;
    private DbsField pnd_Wk_Line_Pnd_Begin_Date;
    private DbsField pnd_Wk_Line_Pnd_Fill2;
    private DbsField pnd_Wk_Line_Pnd_End_Date;

    private DataAccessProgramView vw_conx_Carrier_List;
    private DbsField conx_Carrier_List_Airline_Code;
    private DbsField conx_Carrier_List_Begin_Date;

    private DbsGroup conx_Carrier_List__R_Field_3;
    private DbsField conx_Carrier_List_Pnd_B_Ce;
    private DbsField conx_Carrier_List_Pnd_B_Yy;
    private DbsField conx_Carrier_List_Pnd_B_Mm;
    private DbsField conx_Carrier_List_Pnd_B_Dd;
    private DbsField conx_Carrier_List_End_Date;

    private DbsGroup conx_Carrier_List__R_Field_4;
    private DbsField conx_Carrier_List_Pnd_E_Ce;
    private DbsField conx_Carrier_List_Pnd_E_Yy;
    private DbsField conx_Carrier_List_Pnd_E_Mm;
    private DbsField conx_Carrier_List_Pnd_E_Dd;
    private DbsField conx_Carrier_List_Airline_Name;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        //Data Areas
        gdaFsgsys = GdaFsgsys.getInstance(getCallnatLevel());
        registerRecord(gdaFsgsys);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Command_Compress = localVariables.newFieldInRecord("pnd_Command_Compress", "#COMMAND-COMPRESS", FieldType.STRING, 5);

        pnd_Command_Compress__R_Field_1 = localVariables.newGroupInRecord("pnd_Command_Compress__R_Field_1", "REDEFINE", pnd_Command_Compress);
        pnd_Command_Compress_Pnd_C1 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C1", "#C1", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C2 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C2", "#C2", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C3 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C3", "#C3", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C4 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C4", "#C4", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C5 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C5", "#C5", FieldType.STRING, 1);
        pnd_Display_Half1 = localVariables.newFieldArrayInRecord("pnd_Display_Half1", "#DISPLAY-HALF1", FieldType.STRING, 38, new DbsArrayController(1, 
            15, 1, 2));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_Rec_Found = localVariables.newFieldInRecord("pnd_Rec_Found", "#REC-FOUND", FieldType.STRING, 1);
        pnd_Wk_Line = localVariables.newFieldInRecord("pnd_Wk_Line", "#WK-LINE", FieldType.STRING, 38);

        pnd_Wk_Line__R_Field_2 = localVariables.newGroupInRecord("pnd_Wk_Line__R_Field_2", "REDEFINE", pnd_Wk_Line);
        pnd_Wk_Line_Pnd_Aircd = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Aircd", "#AIRCD", FieldType.STRING, 2);
        pnd_Wk_Line_Pnd_Fill = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Fill", "#FILL", FieldType.STRING, 1);
        pnd_Wk_Line_Pnd_Airline_Name = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Airline_Name", "#AIRLINE-NAME", FieldType.STRING, 23);
        pnd_Wk_Line_Pnd_Fill1 = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Fill1", "#FILL1", FieldType.STRING, 1);
        pnd_Wk_Line_Pnd_Begin_Date = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Begin_Date", "#BEGIN-DATE", FieldType.STRING, 5);
        pnd_Wk_Line_Pnd_Fill2 = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_Fill2", "#FILL2", FieldType.STRING, 1);
        pnd_Wk_Line_Pnd_End_Date = pnd_Wk_Line__R_Field_2.newFieldInGroup("pnd_Wk_Line_Pnd_End_Date", "#END-DATE", FieldType.STRING, 5);

        vw_conx_Carrier_List = new DataAccessProgramView(new NameInfo("vw_conx_Carrier_List", "CONX-CARRIER-LIST"), "ASA_CONX_CARRIER_VALIDATION", "ASA_CONX_CARRIER");
        conx_Carrier_List_Airline_Code = vw_conx_Carrier_List.getRecord().newFieldInGroup("conx_Carrier_List_Airline_Code", "AIRLINE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AIRLINE_CODE");
        conx_Carrier_List_Begin_Date = vw_conx_Carrier_List.getRecord().newFieldInGroup("conx_Carrier_List_Begin_Date", "BEGIN-DATE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BEGIN_DATE");

        conx_Carrier_List__R_Field_3 = vw_conx_Carrier_List.getRecord().newGroupInGroup("conx_Carrier_List__R_Field_3", "REDEFINE", conx_Carrier_List_Begin_Date);
        conx_Carrier_List_Pnd_B_Ce = conx_Carrier_List__R_Field_3.newFieldInGroup("conx_Carrier_List_Pnd_B_Ce", "#B-CE", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_B_Yy = conx_Carrier_List__R_Field_3.newFieldInGroup("conx_Carrier_List_Pnd_B_Yy", "#B-YY", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_B_Mm = conx_Carrier_List__R_Field_3.newFieldInGroup("conx_Carrier_List_Pnd_B_Mm", "#B-MM", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_B_Dd = conx_Carrier_List__R_Field_3.newFieldInGroup("conx_Carrier_List_Pnd_B_Dd", "#B-DD", FieldType.STRING, 2);
        conx_Carrier_List_End_Date = vw_conx_Carrier_List.getRecord().newFieldInGroup("conx_Carrier_List_End_Date", "END-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "END_DATE");

        conx_Carrier_List__R_Field_4 = vw_conx_Carrier_List.getRecord().newGroupInGroup("conx_Carrier_List__R_Field_4", "REDEFINE", conx_Carrier_List_End_Date);
        conx_Carrier_List_Pnd_E_Ce = conx_Carrier_List__R_Field_4.newFieldInGroup("conx_Carrier_List_Pnd_E_Ce", "#E-CE", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_E_Yy = conx_Carrier_List__R_Field_4.newFieldInGroup("conx_Carrier_List_Pnd_E_Yy", "#E-YY", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_E_Mm = conx_Carrier_List__R_Field_4.newFieldInGroup("conx_Carrier_List_Pnd_E_Mm", "#E-MM", FieldType.STRING, 2);
        conx_Carrier_List_Pnd_E_Dd = conx_Carrier_List__R_Field_4.newFieldInGroup("conx_Carrier_List_Pnd_E_Dd", "#E-DD", FieldType.STRING, 2);
        conx_Carrier_List_Airline_Name = vw_conx_Carrier_List.getRecord().newFieldInGroup("conx_Carrier_List_Airline_Name", "AIRLINE-NAME", FieldType.STRING, 
            25, RepeatingFieldStrategy.None, "AIRLINE_NAME");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_conx_Carrier_List.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fscd0nor() throws Exception
    {
        super("Fscd0nor", true);
        initializeFields();
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setKeys(ControlKeys.PF1);                                                                                                                                         //Natural: SET KEY PF1 PF2 PF3 PF8 PF24
        setKeys(ControlKeys.PF2);
        setKeys(ControlKeys.PF3);
        setKeys(ControlKeys.PF8);
        setKeys(ControlKeys.PF24);
        gdaFsgsys.getPnd_Command().reset();                                                                                                                               //Natural: RESET #COMMAND
        gdaFsgsys.getPnd_Program().setValue(Global.getPROGRAM());                                                                                                         //Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_J.setValue(1);                                                                                                                                                //Natural: ASSIGN #J = 1
        vw_conx_Carrier_List.startDatabaseRead                                                                                                                            //Natural: READ CONX-CARRIER-LIST BY AIRLINE-CODE
        (
        "READ01",
        new Oc[] { new Oc("AIRLINE_CODE", "ASC") }
        );
        READ01:
        while (condition(vw_conx_Carrier_List.readNextRow("READ01")))
        {
            if (condition(conx_Carrier_List_Airline_Code.notEquals(" ")))                                                                                                 //Natural: IF AIRLINE-CODE NE ' ' THEN
            {
                pnd_Rec_Found.setValue("Y");                                                                                                                              //Natural: ASSIGN #REC-FOUND = 'Y'
                pnd_Wk_Line_Pnd_Aircd.setValue(conx_Carrier_List_Airline_Code);                                                                                           //Natural: ASSIGN #AIRCD = AIRLINE-CODE
                pnd_Wk_Line_Pnd_Begin_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, conx_Carrier_List_Pnd_B_Mm, "/", conx_Carrier_List_Pnd_B_Yy));        //Natural: COMPRESS #B-MM '/' #B-YY INTO #BEGIN-DATE LEAVING NO SPACE
                pnd_Wk_Line_Pnd_End_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, conx_Carrier_List_Pnd_E_Mm, "/", conx_Carrier_List_Pnd_E_Yy));          //Natural: COMPRESS #E-MM '/' #E-YY INTO #END-DATE LEAVING NO SPACE
                pnd_Wk_Line_Pnd_Airline_Name.setValue(conx_Carrier_List_Airline_Name);                                                                                    //Natural: ASSIGN #AIRLINE-NAME = AIRLINE-NAME
                pnd_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  (0540)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cnt.lessOrEqual(28)))                                                                                                                       //Natural: IF #CNT <= 28 THEN
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                if (condition(pnd_I.equals(15)))                                                                                                                          //Natural: IF #I = 15 THEN
                {
                    pnd_J.setValue(2);                                                                                                                                    //Natural: ASSIGN #J = 2
                    pnd_I.setValue(1);                                                                                                                                    //Natural: ASSIGN #I = 1
                    //*  (1300)
                }                                                                                                                                                         //Natural: END-IF
                pnd_Display_Half1.getValue(pnd_I,pnd_J).setValue(pnd_Wk_Line);                                                                                            //Natural: ASSIGN #DISPLAY-HALF1 ( #I, #J ) = #WK-LINE
                pnd_Wk_Line.reset();                                                                                                                                      //Natural: RESET #WK-LINE
                //*  (0640)
            }                                                                                                                                                             //Natural: END-IF
            //*  (0530)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *******************************************************************
        //* *DISPLAY PROCESSING
        //* *******************************************************************
        if (condition(pnd_Rec_Found.notEquals("Y")))                                                                                                                      //Natural: IF #REC-FOUND NE 'Y' THEN
        {
            pnd_Display_Half1.getValue(4,1).setValue("NO DATA IN CONNECTION CARRIER TABLE");                                                                              //Natural: MOVE 'NO DATA IN CONNECTION CARRIER TABLE' TO #DISPLAY-HALF1 ( 4,1 )
                                                                                                                                                                          //Natural: PERFORM DISPLAY
            sub_Display();
            if (condition(Global.isEscape())) {return;}
            gdaFsgsys.getPnd_Command_Prev().setValue("TBLC");                                                                                                             //Natural: ASSIGN #COMMAND-PREV = 'TBLC'
            Global.setFetchProgram(DbsUtil.getBlType("FSTBLNOI"));                                                                                                        //Natural: FETCH 'FSTBLNOI'
            if (condition(Global.isEscape())) return;
            //*  (0770)
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Display_Half1.getValue(15,1).setValue("END OF REPORT");                                                                                                       //Natural: ASSIGN #DISPLAY-HALF1 ( 15,1 ) = 'END OF REPORT'
                                                                                                                                                                          //Natural: PERFORM DISPLAY
        sub_Display();
        if (condition(Global.isEscape())) {return;}
        gdaFsgsys.getPnd_Command_Prev().setValue("TBLC");                                                                                                                 //Natural: ASSIGN #COMMAND-PREV = 'TBLC'
        Global.setFetchProgram(DbsUtil.getBlType("FSTBLNOI"));                                                                                                            //Natural: FETCH 'FSTBLNOI'
        if (condition(Global.isEscape())) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY
    }
    private void sub_Display() throws Exception                                                                                                                           //Natural: DISPLAY
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("FSCD0NOR|sub_Display");
        while(true)
        {
            try
            {
                while (whileTrue)                                                                                                                                         //Natural: INPUT WITH TEXT #MESSAGE-TEXT ( CD = YE ) USING MAP 'FSCD0NOM'
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), Fscd0nom.class, getCurrentProcessState(), getLocalMethod(), "Input_Fscd0nom", this, new 
                        InputWithText(gdaFsgsys.getPnd_Message_Text()), null, false);
                    if (Global.isEscapeRoutine()) break; if (!Global.isHelpRoutineRan()) break;
                }
                gdaFsgsys.getPnd_Message_Text().reset();                                                                                                                  //Natural: RESET #MESSAGE-TEXT #I #CNT #DISPLAY-HALF1 ( *,* )
                pnd_I.reset();
                pnd_Cnt.reset();
                pnd_Display_Half1.getValue("*","*").reset();
                //* ***************************************************************
                //* *CHECK PF KEYS
                //* ***************************************************************
                //*  PF1 = SYSTEM HELP REQUEST
                if (condition(Global.getPF_KEY().equals("PF1")))                                                                                                          //Natural: IF *PF-KEY = 'PF1' THEN
                {
                    //*        'FSSD2NOI'
                    gdaFsgsys.getPnd_Command().setValue("TBLC");                                                                                                          //Natural: ASSIGN #COMMAND = 'TBLC'
                    Map.reinputUsingHelp(new ReinputMark(gdaFsgsys.getPnd_Command(), true));                                                                              //Natural: REINPUT USING HELP MARK *#COMMAND
                    if (condition(getLocalMethod() != Map.getInputMethod())) return;
                    //*  (0940)
                }                                                                                                                                                         //Natural: END-IF
                //*  PF2 = PREVIOUS FUNCTION
                if (condition(Global.getPF_KEY().equals("PF2")))                                                                                                          //Natural: IF *PF-KEY = 'PF2'
                {
                    gdaFsgsys.getPnd_Command().setValue(gdaFsgsys.getPnd_Command_Prev());                                                                                 //Natural: ASSIGN #COMMAND = #COMMAND-PREV
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBLC");                                                                                                     //Natural: ASSIGN #COMMAND-PREV = 'TBLC'
                    DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                         //Natural: PERFORM FSSD1NOI
                    //*  (0980)
                }                                                                                                                                                         //Natural: END-IF
                //*  PF3 = EXIT THIS FUNCTION
                if (condition(Global.getPF_KEY().equals("PF3")))                                                                                                          //Natural: IF *PF-KEY = 'PF3'
                {
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBLC");                                                                                                     //Natural: ASSIGN #COMMAND-PREV = 'TBLC'
                    Global.setFetchProgram(DbsUtil.getBlType("FSTBLNOI"));                                                                                                //Natural: FETCH 'FSTBLNOI'
                    if (condition(Global.isEscape())) return;
                    //*  (1030)
                }                                                                                                                                                         //Natural: END-IF
                //*  PF8 = PRINT THIS LIST
                if (condition(Global.getPF_KEY().equals("PF8")))                                                                                                          //Natural: IF *PF-KEY = 'PF8'
                {
                    gdaFsgsys.getPnd_Print_Module().setValue("FSCD1NOR");                                                                                                 //Natural: ASSIGN #PRINT-MODULE = 'FSCD1NOR'
                    gdaFsgsys.getPnd_Print_Return().setValue("TBL");                                                                                                      //Natural: ASSIGN #PRINT-RETURN = 'TBL'
                    gdaFsgsys.getPnd_Job_Number().setValue("UFS934M");                                                                                                    //Natural: ASSIGN #JOB-NUMBER = 'UFS934M'
                    Global.setFetchProgram(DbsUtil.getBlType("FSSD3NOI"));                                                                                                //Natural: FETCH 'FSSD3NOI'
                    if (condition(Global.isEscape())) return;
                    //*  (1070)
                }                                                                                                                                                         //Natural: END-IF
                //*  PF24 = EXIT SOFRS
                if (condition(Global.getPF_KEY().equals("PF24")))                                                                                                         //Natural: IF *PF-KEY = 'PF24' THEN
                {
                    Fssd4noi.perform(getCurrentProcessState());                                                                                                           //Natural: PERFORM FSSD4NOI

                    if (condition(Global.isEscape())) {return;}

                    if (condition(Map.getDoInput())) { return; }
                    //*  (1130)
                }                                                                                                                                                         //Natural: END-IF
                //* *******************************************************************
                //* * ELIMINATE SPACES; LEFT JUSTIFY THE COMMAND FIELD; CHECK IF
                //* * REQUESTING ANOTHER SOFRS FUNCTION.
                //* *******************************************************************
                pnd_Command_Compress.reset();                                                                                                                             //Natural: RESET #COMMAND-COMPRESS
                pnd_Command_Compress.setValue(gdaFsgsys.getPnd_Command());                                                                                                //Natural: ASSIGN #COMMAND-COMPRESS = #COMMAND
                pnd_Command_Compress.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Command_Compress_Pnd_C1, pnd_Command_Compress_Pnd_C2,                   //Natural: COMPRESS #C1 #C2 #C3 #C4 #C5 INTO #COMMAND-COMPRESS LEAVING NO SPACE
                    pnd_Command_Compress_Pnd_C3, pnd_Command_Compress_Pnd_C4, pnd_Command_Compress_Pnd_C5));
                gdaFsgsys.getPnd_Command().setValue(pnd_Command_Compress);                                                                                                //Natural: ASSIGN #COMMAND = #COMMAND-COMPRESS
                //*  IF A COMMAND IS ENTERED ACTIVATE SYSTEM ROUTINE
                if (condition(gdaFsgsys.getPnd_Command().notEquals(" ")))                                                                                                 //Natural: IF #COMMAND NE ' '
                {
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBLC");                                                                                                     //Natural: ASSIGN #COMMAND-PREV = 'TBLC'
                    DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                         //Natural: PERFORM FSSD1NOI
                    //*  (1240)
                }                                                                                                                                                         //Natural: END-IF
                //*  (0880)
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
