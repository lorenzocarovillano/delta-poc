/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:40 PM
**        * FROM NATURAL PROGRAM : Fssd3noi
************************************************************
**        * FILE NAME            : Fssd3noi.java
**        * CLASS NAME           : Fssd3noi
**        * INSTANCE NAME        : Fssd3noi
************************************************************
************************************************************************
* FSSD3NOI
*
* THIS MODULE FUNCTIONS AS THE JCL PRINT MODULE FOR SOFRS
* WHEN A SOFRS FUNCTION NEEDS TO KICK OFF A RJE PRINT JOB, IT ACTIVATES
* THIS FUNCTION, THIS FUNCTION IN TURN SETS UP THE JCL AND CALLS RJE
* TO ACTIVATE THE PRINT.
* THIS FUNCTION THEN RETURNS TO THE REQUESTING FUNCTION.
*
* MODIFIED ON 08/08/95 BY EDUARDO M BRITES (TRIAD DATA INC.)
*   - HAVING PROBLEMS WITH #RJE-DATA FROM FSGB0NOR/FSGB1NOR. AFTER
*     RJE THE BATCH PROGRAM FSGB2NOR WAS NOT READING THE ALL DATA
*     STACKED BY RJE. WORKING TOGETHER WITH LOUIZ DIAZ HE DECIDED THAT
*     THE PARAMETER IM=D WAS MISSING.
*
* MODIFIED ON 08/22/95 BY EDUARDO M BRITES (TRIAD DATA INC.)
*   - ALLOW THE USE OF THIS PROGRAM TO ACCEPT THE PRINTER ID AND
*     VALIDATE IT WITHOUT SUBMITING THE RJE. THIS IS TRUE ONLY
*     IF THIS PROGRAM IS CALLED VIA A FETCH RETURN STATEMENT (IN
*     THIS CASE *LEVEL SHOULD BE > 1) AND THE JOB NUMBER IS NULL.
*     IF THE JOB NUMBER IS > SPACES, THEN IT MEANS CARRY ON WITH RJE
*     BUT EXIT THIS PROGRAM BY RETURNING TO THE CALLING PROGRAM AT THE
*     STATEMENT AFTER THE FETCH RETURN. IF THE #PRINT-RETURN FIELD IS
*     > THAN SPACES THEN EXIT THIS PROGRAM BY PROCESSING THE FUNCTION
*     IN #PRINT-RETURN. IF THIS PROGRAM IS PROCESSED AT *LEVEL > 1,
*     WHEN PF3 IS PRESSED, THIS PROGRAM WILL EXIT BY RETURNING TO THE
*     CALLING PROGRAM AT THE STATEMENT AFTER THE FETCH RETURN.
*     IN OTHER CASES IT WILL BE PROCESSED THE WAY IT WAS INITIALLY
*     CODE - IE RETURN TO THE FUNCTION IN #PRINT-RETURN WHICH USUALLY
*     IS THE FUNCTION AT THE TIME THIS PROGRAM HAS BEEN FETCHED.
************************************************************************
*
*

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fssd3noi extends BLNatBase
{
    // Data Areas
    private GdaFsgsys gdaFsgsys;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Parm;

    private DbsGroup pnd_Parm__R_Field_1;
    private DbsField pnd_Parm_Pnd_System;
    private DbsField pnd_Parm_Pnd_Cond_Code;
    private DbsField pnd_Rtn_Code;
    private DbsField pnd_Addr_Area;

    private DbsGroup pnd_Addr_Area__R_Field_2;
    private DbsField pnd_Addr_Area_Pnd_Filler;
    private DbsField pnd_Addr_Area_Pnd_Max_Lines;
    private DbsField pnd_Lgth;
    private DbsField pnd_Tid;
    private DbsField pnd_Jclline1;
    private DbsField pnd_Jclline2;
    private DbsField pnd_Jclline3;
    private DbsField pnd_Jclline4;
    private DbsField pnd_Jclline5;
    private DbsField pnd_Jclline6;
    private DbsField pnd_Jclline7;
    private DbsField pnd_Jclline8;
    private DbsField pnd_Jclline9;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Length;
    private DbsField pnd_R;
    private DbsField pnd_Roll_Time;
    private DbsField pnd_Alpha_Addr;
    private DbsField pnd_Command_Check;

    private DbsGroup pnd_Command_Check__R_Field_3;
    private DbsField pnd_Command_Check_Pnd_Command_Short;
    private DbsField pnd_Command_Check_Pnd_Filler;
    private int getchrReturnCode;
    private int whereReturnCode;
    private int rjeReturnCode;
    private int roloutReturnCode;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        //Data Areas
        gdaFsgsys = GdaFsgsys.getInstance(getCallnatLevel());
        registerRecord(gdaFsgsys);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Parm = localVariables.newFieldInRecord("pnd_Parm", "#PARM", FieldType.STRING, 2);

        pnd_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm__R_Field_1", "REDEFINE", pnd_Parm);
        pnd_Parm_Pnd_System = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_System", "#SYSTEM", FieldType.STRING, 1);
        pnd_Parm_Pnd_Cond_Code = pnd_Parm__R_Field_1.newFieldInGroup("pnd_Parm_Pnd_Cond_Code", "#COND_CODE", FieldType.STRING, 1);
        pnd_Rtn_Code = localVariables.newFieldInRecord("pnd_Rtn_Code", "#RTN-CODE", FieldType.BINARY, 4);
        pnd_Addr_Area = localVariables.newFieldInRecord("pnd_Addr_Area", "#ADDR-AREA", FieldType.STRING, 8);

        pnd_Addr_Area__R_Field_2 = localVariables.newGroupInRecord("pnd_Addr_Area__R_Field_2", "REDEFINE", pnd_Addr_Area);
        pnd_Addr_Area_Pnd_Filler = pnd_Addr_Area__R_Field_2.newFieldInGroup("pnd_Addr_Area_Pnd_Filler", "#FILLER", FieldType.STRING, 6);
        pnd_Addr_Area_Pnd_Max_Lines = pnd_Addr_Area__R_Field_2.newFieldInGroup("pnd_Addr_Area_Pnd_Max_Lines", "#MAX-LINES", FieldType.BINARY, 2);
        pnd_Lgth = localVariables.newFieldInRecord("pnd_Lgth", "#LGTH", FieldType.BINARY, 2);
        pnd_Tid = localVariables.newFieldInRecord("pnd_Tid", "#TID", FieldType.BINARY, 2);
        pnd_Jclline1 = localVariables.newFieldInRecord("pnd_Jclline1", "#JCLLINE1", FieldType.STRING, 80);
        pnd_Jclline2 = localVariables.newFieldInRecord("pnd_Jclline2", "#JCLLINE2", FieldType.STRING, 80);
        pnd_Jclline3 = localVariables.newFieldInRecord("pnd_Jclline3", "#JCLLINE3", FieldType.STRING, 80);
        pnd_Jclline4 = localVariables.newFieldInRecord("pnd_Jclline4", "#JCLLINE4", FieldType.STRING, 80);
        pnd_Jclline5 = localVariables.newFieldInRecord("pnd_Jclline5", "#JCLLINE5", FieldType.STRING, 80);
        pnd_Jclline6 = localVariables.newFieldInRecord("pnd_Jclline6", "#JCLLINE6", FieldType.STRING, 80);
        pnd_Jclline7 = localVariables.newFieldInRecord("pnd_Jclline7", "#JCLLINE7", FieldType.STRING, 80);
        pnd_Jclline8 = localVariables.newFieldInRecord("pnd_Jclline8", "#JCLLINE8", FieldType.STRING, 80);
        pnd_Jclline9 = localVariables.newFieldInRecord("pnd_Jclline9", "#JCLLINE9", FieldType.STRING, 80);
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.BINARY, 4);
        pnd_Length = localVariables.newFieldInRecord("pnd_Length", "#LENGTH", FieldType.BINARY, 2);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_Roll_Time = localVariables.newFieldInRecord("pnd_Roll_Time", "#ROLL-TIME", FieldType.BINARY, 2);
        pnd_Alpha_Addr = localVariables.newFieldInRecord("pnd_Alpha_Addr", "#ALPHA-ADDR", FieldType.STRING, 4);
        pnd_Command_Check = localVariables.newFieldInRecord("pnd_Command_Check", "#COMMAND-CHECK", FieldType.STRING, 5);

        pnd_Command_Check__R_Field_3 = localVariables.newGroupInRecord("pnd_Command_Check__R_Field_3", "REDEFINE", pnd_Command_Check);
        pnd_Command_Check_Pnd_Command_Short = pnd_Command_Check__R_Field_3.newFieldInGroup("pnd_Command_Check_Pnd_Command_Short", "#COMMAND-SHORT", FieldType.STRING, 
            4);
        pnd_Command_Check_Pnd_Filler = pnd_Command_Check__R_Field_3.newFieldInGroup("pnd_Command_Check_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fssd3noi() throws Exception
    {
        super("Fssd3noi", true);
        initializeFields();
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fssd3noi|Main");
        while(true)
        {
            try
            {
                //* ***********************************************************************
                //*  SET PF KEYS, STORE CURRENT PROGRAM NAME IN GLOBAL, INPUT MAP REQUEST-
                //*  ING PRINTER ADDRESS, AUDIT FOR PF3 FROM MAP
                //* ***********************************************************************
                setKeysAll();                                                                                                                                             //Natural: SET KEY ALL
                gdaFsgsys.getPnd_Program().setValue(Global.getPROGRAM());                                                                                                 //Natural: MOVE *PROGRAM TO #PROGRAM
                while (whileTrue)                                                                                                                                         //Natural: INPUT USING MAP 'FSSD3NOM'
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), Fssd3nom.class, getCurrentProcessState(), getLocalMethod(), "Input_Fssd3nom", this, "", 
                        null, false);
                    if (Global.isEscapeRoutine()) break; if (!Global.isHelpRoutineRan()) break;
                }
                if (condition(Global.getPF_KEY().equals("PF3")))                                                                                                          //Natural: IF *PF-KEY = 'PF3'
                {
                    gdaFsgsys.getPnd_Message_Text().setValue("PRINT REQUEST CANCELLED BY YOU");                                                                           //Natural: ASSIGN #MESSAGE-TEXT = 'PRINT REQUEST CANCELLED BY YOU'
                    if (condition(Global.getLEVEL().greater(1)))                                                                                                          //Natural: IF *LEVEL > 1
                    {
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(gdaFsgsys.getPnd_Print_Return().notEquals(" ")))                                                                                        //Natural: IF #PRINT-RETURN NE ' '
                    {
                                                                                                                                                                          //Natural: PERFORM EXIT-BY-COMMAND
                        sub_Exit_By_Command();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF THROUGH HERE THEN IT MEANS CERTAIN FIELDS WERE NOT SETUP PROPERLY
                    //*  FOR THE PROGRAM TO TAKE THE PROPER PRECEDURE. THE CALLING PROGRAM
                    //*  SHOULD BE CHECKED - THE #PRINT-RETURN OR THE #JOB-NUMBER FIELDS.
                    //*  ALSO THE *LEVEL TO MAKE SURE THIS PROGRAM IS RUNNING BY USING THE
                    //*  CORRECT EXECUTE STATEMENT (FETCH OR FETCH RETURN).
                    //* (0990)
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!Global.getPF_KEY().equals("ENTR")))                                                                                                        //Natural: IF *PF-KEY NE 'ENTR'
                {
                    Map.reinput(new ReinputWithText("INVALID PF KEY SELECTED", new FieldAttributes("CD=YE")), new ReinputAlarm());                                        //Natural: REINPUT ( CD = YE ) 'INVALID PF KEY SELECTED' ALARM
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  AUDIT FOR PRINTER, IF NO PF3 ENTERED, MUST HAVE PRINTER AND PRINTER
                //*  MUST BE 4 NUMBERICS
                //*  USE ROUTINE GETCHR TO DETERINE IF INPUT ADDRESS IS A VALID TID
                //*  IF PRINTER OK, MOVE TO NUMERIC FIELD TO PASS TO RJE
                //* ***********************************************************************
                if (condition(gdaFsgsys.getPnd_Printer_Addr().equals(getZero())))                                                                                         //Natural: IF #PRINTER-ADDR = 0
                {
                    Map.reinput(new ReinputWithText("NEED PRINTER ID", new FieldAttributes("CD=YE")), new ReinputMark(gdaFsgsys.getPnd_Printer_Addr(),                    //Natural: REINPUT ( CD = YE ) 'NEED PRINTER ID' MARK *#PRINTER-ADDR ALARM
                        new FieldAttributes("CD=YE"), true), new ReinputAlarm());
                    //* (1320)
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rtn_Code.reset();                                                                                                                                     //Natural: RESET #RTN-CODE #ADDR-AREA
                pnd_Addr_Area.reset();
                pnd_Lgth.setValue(8);                                                                                                                                     //Natural: ASSIGN #LGTH = 8
                pnd_Tid.setValue(gdaFsgsys.getPnd_Printer_Addr());                                                                                                        //Natural: MOVE #PRINTER-ADDR TO #TID
                getchrReturnCode = DbsUtil.callExternalProgram("GETCHR",pnd_Rtn_Code,pnd_Addr_Area,pnd_Lgth,pnd_Tid);                                                     //Natural: CALL 'GETCHR' #RTN-CODE #ADDR-AREA #LGTH #TID
                if (condition(pnd_Rtn_Code.equals(getZero()) && pnd_Addr_Area_Pnd_Max_Lines.equals(getZero())))                                                           //Natural: IF #RTN-CODE = 0 AND #MAX-LINES = 0 THEN
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    Map.reinput(new ReinputWithText("INVALID PRINTER ID", new FieldAttributes("CD=YE")), new ReinputMark(gdaFsgsys.getPnd_Printer_Addr(),                 //Natural: REINPUT ( CD = YE ) 'INVALID PRINTER ID' MARK *#PRINTER-ADDR ALARM
                        new FieldAttributes("CD=YE"), true), new ReinputAlarm());
                    //*  (1440)
                }                                                                                                                                                         //Natural: END-IF
                //*  CALLED WITH FETCH RETURN. THE PURPOSE IS TO INPUT
                if (condition(Global.getLEVEL().greater(1)))                                                                                                              //Natural: IF *LEVEL > 1
                {
                    //*  PRINTER NBR AND VALIDATE IT ONLY.
                    if (condition(gdaFsgsys.getPnd_Job_Number().equals(" ") || gdaFsgsys.getPnd_Job_Number().equals("0000000")))                                          //Natural: IF #JOB-NUMBER = ' ' OR = '0000000'
                    {
                        //*  THE USE OF THIS PROGRAM FOR RJE IS NOT SUITABLE, PLEASE EXIT.
                        //*  EXIT TO THE FUNCTION IN #PRINT-RETURN.
                        if (condition(gdaFsgsys.getPnd_Print_Return().notEquals(" ")))                                                                                    //Natural: IF #PRINT-RETURN NE ' '
                        {
                                                                                                                                                                          //Natural: PERFORM EXIT-BY-COMMAND
                            sub_Exit_By_Command();
                            if (condition(Global.isEscape())) {return;}
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        //*  RETURN TO THE CALLING PGM AT THE FETCH RETURN.
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Alpha_Addr.setValue(gdaFsgsys.getPnd_Printer_Addr());                                                                                                 //Natural: MOVE #PRINTER-ADDR TO #ALPHA-ADDR
                //* ***********************************************************************
                //*  SET UP JCL.  USE SYMBOLICS ON SOME FIELDS IN ORDER TO SUBSTITUTE THEM
                //*  LATER.
                //* ***********************************************************************
                pnd_Jclline1.setValue("//JJJJJ   JOB ,'JJJJJ',CLASS=@,MSGCLASS=&,MSGLEVEL=1");                                                                            //Natural: MOVE '//JJJJJ   JOB ,"JJJJJ",CLASS=@,MSGCLASS=&,MSGLEVEL=1' TO #JCLLINE1
                pnd_Jclline2.setValue("//    EXEC !,SYSOUT=%,PARM='PRINTER=(PPPP),IM=D',STATUS=*");                                                                       //Natural: MOVE '//    EXEC !,SYSOUT=%,PARM="PRINTER=(PPPP),IM=D",STATUS=*' TO #JCLLINE2
                pnd_Jclline3.setValue("//*");                                                                                                                             //Natural: MOVE '//*' TO #JCLLINE3
                pnd_Jclline4.setValue("//SYSIN DD *");                                                                                                                    //Natural: MOVE '//SYSIN DD *' TO #JCLLINE4
                pnd_Jclline5.setValue("SOFRSA,JJJJJ,JJJJJ");                                                                                                              //Natural: MOVE 'SOFRSA,JJJJJ,JJJJJ' TO #JCLLINE5
                pnd_Jclline6.setValue("NNNNNNNN");                                                                                                                        //Natural: MOVE 'NNNNNNNN' TO #JCLLINE6
                //* ***********************************************************************
                //*  IF THERE IS NO DATA TO BE PASSED TO PRINT PROGRAM, PUT THE 'FIN' ON
                //*  THE 8TH LINE OF JCL.  OTHERWISE, PUT IT ON THE 9TH
                //* ***********************************************************************
                if (condition(gdaFsgsys.getPnd_Rje_Data().equals(" ")))                                                                                                   //Natural: IF #RJE-DATA = ' '
                {
                    pnd_Jclline7.setValue("FIN");                                                                                                                         //Natural: MOVE 'FIN' TO #JCLLINE7
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Jclline7.setValue(gdaFsgsys.getPnd_Rje_Data());                                                                                                   //Natural: MOVE #RJE-DATA TO #JCLLINE7
                    pnd_Jclline8.setValue("FIN");                                                                                                                         //Natural: MOVE 'FIN' TO #JCLLINE8
                    //*  (1840)
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  SUBSTITUTE VALUES PASSED IN GLOBAL FSGSYS AND THE ENTER PRINTER ADDR
                //*  FOR SOME OF THE SYMBOLICS USED IN THE JCL
                //*  THE REST OF THE SYMBOLICS ARE SUBSTITUTED BASED ON WHAT SYSTEM THEY'RE
                //*  IN:  TEST, ACCEPTANCE, PRODUCTION
                //* ***********************************************************************
                DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("JJJJJ"), new ExamineReplace(gdaFsgsys.getPnd_Job_Number()));                          //Natural: EXAMINE #JCLLINE1 FOR 'JJJJJ' REPLACE WITH #JOB-NUMBER
                DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("PPPP"), new ExamineReplace(pnd_Alpha_Addr));                                          //Natural: EXAMINE #JCLLINE2 FOR 'PPPP' REPLACE WITH #ALPHA-ADDR
                DbsUtil.examine(new ExamineSource(pnd_Jclline5), new ExamineSearch("JJJJJ"), new ExamineReplace(gdaFsgsys.getPnd_Job_Number()));                          //Natural: EXAMINE #JCLLINE5 FOR 'JJJJJ' REPLACE WITH #JOB-NUMBER
                DbsUtil.examine(new ExamineSource(pnd_Jclline6), new ExamineSearch("NNNNNNNN"), new ExamineReplace(gdaFsgsys.getPnd_Print_Module()));                     //Natural: EXAMINE #JCLLINE6 FOR 'NNNNNNNN' AND REPLACE WITH #PRINT-MODULE
                whereReturnCode = DbsUtil.callExternalProgram("WHERE",pnd_Parm);                                                                                          //Natural: CALL 'WHERE' #PARM
                //* * *INIT-PROGRAM= 'N2TEST'
                //*  IN TEST SYSTEM
                if (condition(pnd_Parm_Pnd_System.equals("T")))                                                                                                           //Natural: IF #SYSTEM = 'T'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("@"), new ExamineReplace("Z"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '@' REPLACE WITH 'Z'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("&"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '&' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("!"), new ExamineReplace("SNBATCH"));                                              //Natural: EXAMINE #JCLLINE2 FOR '!' REPLACE WITH 'SNBATCH'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("%"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE2 FOR '%' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("*"), new ExamineReplace("TEST"));                                                 //Natural: EXAMINE #JCLLINE2 FOR '*' REPLACE WITH 'TEST'
                    //*  (2080)
                }                                                                                                                                                         //Natural: END-IF
                //* * *INIT-PROGRAM= 'NATACC'
                //*  IN ACCEPTANCE SYSTEM
                if (condition(pnd_Parm_Pnd_System.equals("A")))                                                                                                           //Natural: IF #SYSTEM = 'A'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("@"), new ExamineReplace("Z"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '@' REPLACE WITH 'Z'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("&"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '&' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("!"), new ExamineReplace("SNBATCH"));                                              //Natural: EXAMINE #JCLLINE2 FOR '!' REPLACE WITH 'SNBATCH'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("%"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE2 FOR '%' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("*"), new ExamineReplace("ACCP"));                                                 //Natural: EXAMINE #JCLLINE2 FOR '*' REPLACE WITH 'ACCP'
                    //*  (2200)
                }                                                                                                                                                         //Natural: END-IF
                //* * *INIT-PROGRAM= 'N2PROD'
                //*  IN PRODUCTION SYSTEM
                if (condition(pnd_Parm_Pnd_System.equals("P")))                                                                                                           //Natural: IF #SYSTEM = 'P'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("@"), new ExamineReplace("J"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '@' REPLACE WITH 'J'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline1), new ExamineSearch("&"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE1 FOR '&' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("!"), new ExamineReplace("SNBATCH"));                                              //Natural: EXAMINE #JCLLINE2 FOR '!' REPLACE WITH 'SNBATCH'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("%"), new ExamineReplace("X"));                                                    //Natural: EXAMINE #JCLLINE2 FOR '%' REPLACE WITH 'X'
                    DbsUtil.examine(new ExamineSource(pnd_Jclline2), new ExamineSearch("*"), new ExamineReplace("PROD"));                                                 //Natural: EXAMINE #JCLLINE2 FOR '*' REPLACE WITH 'PROD'
                    //*  (2320)
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  DETERMINE THE LENGHT OF THE JCL BASED ON IF THERE IS DATA TO PASS
                //* ***********************************************************************
                if (condition(gdaFsgsys.getPnd_Rje_Data().equals(" ")))                                                                                                   //Natural: IF #RJE-DATA = ' '
                {
                    pnd_Length.setValue(560);                                                                                                                             //Natural: ASSIGN #LENGTH = 560
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Length.setValue(640);                                                                                                                             //Natural: ASSIGN #LENGTH = 640
                    //*  (2490)
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  PREPARE TO SUBMIT THE JCL VIA RJE.  ATTEMPT TO SUBMIT IT UP TO 10
                //*  TIMES.  IF THE RETURN CODE IS A 4, THERE IS CONFLICT FOR THE THREAD,
                //*  SO TRY TO SUBMIT IT AGAIN.  FOR ANY OTHER ERROR, AND FOR RETURN CODE
                //*  4 THAT OCCURS MORE THAN ONCE, SET UP THE AN ERROR MESSAGE.
                //*  USE ROUTINE ROLOUT TO RE-SUBMIT THE JCL.
                //*  THE ERROR MSG OR THE 'PRINT INITIATED' MSG IS PASSED TO THE ACTIVATING
                //*  FUNCTION VIA THE SYSTEM GLOBAL
                //* ***********************************************************************
                //*  ALLOW 10 TIMES SUBMIT TO RJE
                pnd_Return_Code.reset();                                                                                                                                  //Natural: RESET #RETURN-CODE
                FOR01:                                                                                                                                                    //Natural: FOR #R = 1 THRU 10
                for (pnd_R.setValue(1); condition(pnd_R.lessOrEqual(10)); pnd_R.nadd(1))
                {
                    rjeReturnCode = DbsUtil.callExternalProgram("RJE",pnd_Return_Code,pnd_Jclline1,pnd_Length);                                                           //Natural: CALL 'RJE' #RETURN-CODE #JCLLINE1 #LENGTH
                    //*  IF ERROR CODE IS 4 FOR RFE CONFLICT
                    if (condition(pnd_Return_Code.equals(4)))                                                                                                             //Natural: IF #RETURN-CODE = 4
                    {
                        //*  SET THE ROLLOUT TIME TO 1 SECOND
                        pnd_Roll_Time.setValue(1);                                                                                                                        //Natural: ASSIGN #ROLL-TIME = 1
                        //*  AND TRY IT AGAIN
                        roloutReturnCode = DbsUtil.callExternalProgram("ROLOUT",pnd_Roll_Time);                                                                           //Natural: CALL 'ROLOUT' #ROLL-TIME
                        //*  IF NO ERROR, OR A DIFFERENT ERROR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  IF NO ERROR WAS RETURNED ESCAPE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  (2720)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (2680)
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(pnd_Return_Code.notEquals(getZero()) || pnd_Parm_Pnd_Cond_Code.notEquals("0")))                                                             //Natural: IF #RETURN-CODE NE 0 OR #COND_CODE NE '0'
                {
                    gdaFsgsys.getPnd_Message_Text().setValue("PRINT FAILED - CONTACT PROGRAMMING");                                                                       //Natural: MOVE 'PRINT FAILED - CONTACT PROGRAMMING' TO #MESSAGE-TEXT
                    //*  (2820)
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Return_Code.equals(getZero()) && pnd_Parm_Pnd_Cond_Code.equals("0")))                                                                   //Natural: IF #RETURN-CODE = 0 AND #COND_CODE = '0'
                {
                    gdaFsgsys.getPnd_Message_Text().setValue("PRINT INITIATED");                                                                                          //Natural: MOVE 'PRINT INITIATED' TO #MESSAGE-TEXT
                    //*  (2880)
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  CLEAN UP THE GLOBAL FIELDS USED, AND SET UP TO GO TO FSSD1NOI
                //*  (OR FSUP6NOU FOR UPDATE FLIGHT DETAIL FUNCTIONS) IN ORDER
                //*  TO RETURN TO THE ACTIVATING FUNCTION
                //* ***********************************************************************
                //*  CALLED WITH FETCH RETURN.
                //*  EXIT TO THE FUNCTION IN #PRINT-RETURN.
                if (condition(Global.getLEVEL().greater(1) && gdaFsgsys.getPnd_Print_Return().equals(" ")))                                                               //Natural: IF *LEVEL > 1 AND #PRINT-RETURN = ' '
                {
                    //*  RETURN TO THE CALLING PGM AT THE FETCH RETURN.
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  WHEN *LEVEL IS 1, #PRINT-RETURN SHOULD ALWAYS BE FILLED.
                                                                                                                                                                          //Natural: PERFORM EXIT-BY-COMMAND
                sub_Exit_By_Command();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  SUBROUTINES
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXIT-BY-COMMAND
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Exit_By_Command() throws Exception                                                                                                                   //Natural: EXIT-BY-COMMAND
    {
        if (BLNatReinput.isReinput()) return;

        gdaFsgsys.getPnd_Rje_Data().reset();                                                                                                                              //Natural: RESET #RJE-DATA #PRINT-MODULE
        gdaFsgsys.getPnd_Print_Module().reset();
        gdaFsgsys.getPnd_Command().setValue(gdaFsgsys.getPnd_Print_Return());                                                                                             //Natural: MOVE #PRINT-RETURN TO #COMMAND #COMMAND-CHECK
        pnd_Command_Check.setValue(gdaFsgsys.getPnd_Print_Return());
        gdaFsgsys.getPnd_Print_Return().reset();                                                                                                                          //Natural: RESET #PRINT-RETURN
        if (condition(pnd_Command_Check_Pnd_Command_Short.equals("MTND") || pnd_Command_Check_Pnd_Command_Short.equals("NEXT") || gdaFsgsys.getPnd_Command().equals("RMRKS")  //Natural: IF #COMMAND-SHORT = 'MTND' OR = 'NEXT' OR #COMMAND = 'RMRKS' OR #COMMAND = 'N' OR #COMMAND = 'R'
            || gdaFsgsys.getPnd_Command().equals("N") || gdaFsgsys.getPnd_Command().equals("R")))
        {
            DbsUtil.terminateApplication("External Subroutine FSUP6NOU is missing from the collection!");                                                                 //Natural: PERFORM FSUP6NOU
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                                 //Natural: PERFORM FSSD1NOI
            //*  (1000)
        }                                                                                                                                                                 //Natural: END-IF
        //*  EXIT-BY-COMMAND
    }

    //
}
