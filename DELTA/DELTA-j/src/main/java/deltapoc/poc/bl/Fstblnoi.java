/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:40 PM
**        * FROM NATURAL PROGRAM : Fstblnoi
************************************************************
**        * FILE NAME            : Fstblnoi.java
**        * CLASS NAME           : Fstblnoi
**        * INSTANCE NAME        : Fstblnoi
************************************************************
*****************************************************************
** THIS IS THE PROGRAM FOR THE TABLE DISPLAYS MENU OF SOFRS
** THIS PROGRAM IS ACTIVATED BY FSMENNOI (MAIN SOFRS MENU)
** LAVONNE COLE 8/7/92
*
* MODIFIED ON 01/10/9P7 BY EDUARDO M BRITES (TRIAD DATA INC.):
*   - NEW OPTION I TO FETCH FSFI0NOR FOR FLIGHTS CROSS REFERENCE.
*
* MODIFIED ON 10/31/97 BY DIANE O'DELL
*   - NEW OPTION J TO FETCH FSFJ0NOR FOR CODESHARE SCHEDULE
* MODIFIED ON 05/17/00 BY TOM LOVETT
*   - ADD AIRLINE CODE AND AIRLINE NAME TO MAP
*
*****************************************************************
**
**

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fstblnoi extends BLNatBase
{
    // Data Areas
    private GdaFsgsys gdaFsgsys;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Command_Compress;

    private DbsGroup pnd_Command_Compress__R_Field_1;
    private DbsField pnd_Command_Compress_Pnd_C1;
    private DbsField pnd_Command_Compress_Pnd_C2;
    private DbsField pnd_Command_Compress_Pnd_C3;
    private DbsField pnd_Command_Compress_Pnd_C4;
    private DbsField pnd_Command_Compress_Pnd_C5;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        //Data Areas
        gdaFsgsys = GdaFsgsys.getInstance(getCallnatLevel());
        registerRecord(gdaFsgsys);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Command_Compress = localVariables.newFieldInRecord("pnd_Command_Compress", "#COMMAND-COMPRESS", FieldType.STRING, 5);

        pnd_Command_Compress__R_Field_1 = localVariables.newGroupInRecord("pnd_Command_Compress__R_Field_1", "REDEFINE", pnd_Command_Compress);
        pnd_Command_Compress_Pnd_C1 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C1", "#C1", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C2 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C2", "#C2", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C3 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C3", "#C3", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C4 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C4", "#C4", FieldType.STRING, 1);
        pnd_Command_Compress_Pnd_C5 = pnd_Command_Compress__R_Field_1.newFieldInGroup("pnd_Command_Compress_Pnd_C5", "#C5", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fstblnoi() throws Exception
    {
        super("Fstblnoi", true);
        initializeFields();
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fstblnoi|Main");
        while(true)
        {
            try
            {
                //* *
                //* *
                //* *************************************************************
                //* * DEFINE THE PF KEYS, EMPTY THE COMMAND LINE
                //* *************************************************************
                //* *
                //* *
                setKeys(ControlKeys.PF1);                                                                                                                                 //Natural: SET KEY PF1 PF2 PF3 PF8 PF24
                setKeys(ControlKeys.PF2);
                setKeys(ControlKeys.PF3);
                setKeys(ControlKeys.PF8);
                setKeys(ControlKeys.PF24);
                gdaFsgsys.getPnd_Command().reset();                                                                                                                       //Natural: RESET #COMMAND
                gdaFsgsys.getPnd_Program().setValue(Global.getPROGRAM());                                                                                                 //Natural: MOVE *PROGRAM TO #PROGRAM
                //* *
                //* ***************************************************************
                //* * DISPLAY THE MENU WITH PROCESSING SELECTIONS AND ANY GLOBAL MSG
                //* * MAP 'FSTBLNOM' SUBFIELD #COMMAND CALLS HELPROUTINE 'FSSD2NOI'
                //* ***************************************************************
                //* *
                //* *
                while (whileTrue)                                                                                                                                         //Natural: INPUT WITH TEXT #MESSAGE-TEXT ( CD = YE ) USING MAP 'FSTBLNOM'
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), Fstblnom.class, getCurrentProcessState(), getLocalMethod(), "Input_Fstblnom", this, new 
                        InputWithText(gdaFsgsys.getPnd_Message_Text()), null, false);
                    if (Global.isEscapeRoutine()) break; if (!Global.isHelpRoutineRan()) break;
                }
                gdaFsgsys.getPnd_Message_Text().reset();                                                                                                                  //Natural: RESET #MESSAGE-TEXT
                //* *
                //* *
                //* ***************************************************************
                //* *CHECK PF KEYS
                //* ***************************************************************
                //* *
                //* *
                //*  PF1 = SYSTEM HELP REQUEST
                if (condition(Global.getPF_KEY().equals("PF1")))                                                                                                          //Natural: IF *PF-KEY = 'PF1'
                {
                    //*        'FSSD2NOI'
                    gdaFsgsys.getPnd_Command().setValue("TBL");                                                                                                           //Natural: MOVE 'TBL' TO #COMMAND
                    Map.reinputUsingHelp(new ReinputMark(gdaFsgsys.getPnd_Command(), true));                                                                              //Natural: REINPUT USING HELP MARK *#COMMAND
                    //*  (0690)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                //*  PF2 = PREVIOUS FUNCTION
                if (condition(Global.getPF_KEY().equals("PF2")))                                                                                                          //Natural: IF *PF-KEY = 'PF2'
                {
                    gdaFsgsys.getPnd_Command().setValue(gdaFsgsys.getPnd_Command_Prev());                                                                                 //Natural: MOVE #COMMAND-PREV TO #COMMAND
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                         //Natural: PERFORM FSSD1NOI
                    //*  (0750)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                //*  PF3 = EXIT (RTN TO MAIN
                if (condition(Global.getPF_KEY().equals("PF3")))                                                                                                          //Natural: IF *PF-KEY = 'PF3'
                {
                    //*        MENU)
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    gdaFsgsys.getPnd_Command().setValue("MAIN");                                                                                                          //Natural: MOVE 'MAIN' TO #COMMAND
                    DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                         //Natural: PERFORM FSSD1NOI
                    //*  (0820)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                if (condition(Global.getPF_KEY().equals("PF8")))                                                                                                          //Natural: IF *PF-KEY = 'PF8'
                {
                    Map.reinput(new ReinputWithText("PRINT NOT AVAILABLE FOR THIS MENU", new FieldAttributes("CD=YE")), new ReinputAlarm());                              //Natural: REINPUT ( CD = YE ) 'PRINT NOT AVAILABLE FOR THIS MENU' SOUND ALARM
                    //*  (0890)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                //*  PF24 = EXIT SOFRS
                if (condition(Global.getPF_KEY().equals("PF24")))                                                                                                         //Natural: IF *PF-KEY = 'PF24'
                {
                    Fssd4noi.perform(getCurrentProcessState());                                                                                                           //Natural: PERFORM FSSD4NOI

                    if (condition(Global.isEscape())) {return;}

                    if (condition(Map.getDoInput())) { return; }
                    //*  (0940)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                //* *************************************************************
                //* * ELIMINATE SPACES; LEFT JUSTIFY THE COMMAND FIELD; AND CHECK
                //* * IF REQUESTING A ANOTHER SOFRS FUNCTION OUTSIDE OF 'TABLE
                //* * DISPLAYS.
                //* *************************************************************
                //* *
                //* *
                pnd_Command_Compress.reset();                                                                                                                             //Natural: RESET #COMMAND-COMPRESS
                pnd_Command_Compress.setValue(gdaFsgsys.getPnd_Command());                                                                                                //Natural: MOVE #COMMAND TO #COMMAND-COMPRESS
                //* *
                pnd_Command_Compress.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Command_Compress_Pnd_C1, pnd_Command_Compress_Pnd_C2,                   //Natural: COMPRESS #C1 #C2 #C3 #C4 #C5 INTO #COMMAND-COMPRESS LEAVING NO SPACE
                    pnd_Command_Compress_Pnd_C3, pnd_Command_Compress_Pnd_C4, pnd_Command_Compress_Pnd_C5));
                //* *
                gdaFsgsys.getPnd_Command().setValue(pnd_Command_Compress);                                                                                                //Natural: MOVE #COMMAND-COMPRESS TO #COMMAND
                //* *
                //* *
                //*  IF MORE THAN ONE-LETTER
                if (condition(pnd_Command_Compress_Pnd_C2.notEquals(" ")))                                                                                                //Natural: IF #C2 NE ' '
                {
                    //*  THE RQST IS FOR ANOTHER
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  ANOTHER FUNCTION.
                    DbsUtil.terminateApplication("External Subroutine FSSD1NOI is missing from the collection!");                                                         //Natural: PERFORM FSSD1NOI
                    //*  (1140)
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //* *
                //* ******************************************************************
                //* * AUDIT THE MENU REQUEST AND FETCH APPROPRIATE PROGRAM
                //* ******************************************************************
                //* *
                //* *
                //* *                                                                                                                                                     //Natural: DECIDE ON FIRST VALUE #COMMAND
                //* *
                short decideConditionsMet263 = 0;                                                                                                                         //Natural: VALUE ' '
                if (condition((gdaFsgsys.getPnd_Command().equals(" "))))
                {
                    decideConditionsMet263++;
                    //*  EQUIPMENT
                    Map.reinput(new ReinputWithText("NEED COMMAND OR PF KEY", new FieldAttributes("CD=YE")), new ReinputAlarm());                                         //Natural: REINPUT ( CD = YE ) 'NEED COMMAND OR PF KEY' AND SOUND ALARM
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'A'
                else if (condition((gdaFsgsys.getPnd_Command().equals("A"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  FLIGHT SCHEDULE
                    Global.setFetchProgram(DbsUtil.getBlType("FSFA0NOR"));                                                                                                //Natural: FETCH 'FSFA0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((gdaFsgsys.getPnd_Command().equals("B"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  REPORT CODES
                    Global.setFetchProgram(DbsUtil.getBlType("FSFB0NOR"));                                                                                                //Natural: FETCH 'FSFB0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                    //* *VALUE 'C'
                    //* *  MOVE 'TBL' TO #COMMAND-PREV
                    //* *  FETCH 'FSFC0NOR'
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'D'
                else if (condition((gdaFsgsys.getPnd_Command().equals("D"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  REGION CODES
                    Global.setFetchProgram(DbsUtil.getBlType("FSFD0NOR"));                                                                                                //Natural: FETCH 'FSFD0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'E'
                else if (condition((gdaFsgsys.getPnd_Command().equals("E"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  ENTITY CODES HAVE BEEN
                    Global.setFetchProgram(DbsUtil.getBlType("FSFE0NOR"));                                                                                                //Natural: FETCH 'FSFE0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'F'
                else if (condition((gdaFsgsys.getPnd_Command().equals("F"))))
                {
                    decideConditionsMet263++;
                    //*  DELETED FROM SOFRS
                    //*  AIRPORT CODES
                    Map.reinput(new ReinputWithText("INVALID COMMAND, REENTER", new FieldAttributes("CD=YE")), new ReinputAlarm());                                       //Natural: REINPUT ( CD = YE ) 'INVALID COMMAND, REENTER' SOUND ALARM
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'G'
                else if (condition((gdaFsgsys.getPnd_Command().equals("G"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  SHIP CAPACITY
                    Global.setFetchProgram(DbsUtil.getBlType("FSFG0NOR"));                                                                                                //Natural: FETCH 'FSFG0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'H'
                else if (condition((gdaFsgsys.getPnd_Command().equals("H"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  SHIP CAPACITY
                    Global.setFetchProgram(DbsUtil.getBlType("FSFH0NOR"));                                                                                                //Natural: FETCH 'FSFH0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((gdaFsgsys.getPnd_Command().equals("I"))))
                {
                    decideConditionsMet263++;
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  CONNECTION CARRIERS   DK 3/00
                    Global.setFetchProgram(DbsUtil.getBlType("FSFI0NOR"));                                                                                                //Natural: FETCH 'FSFI0NOR'
                    if (condition(Global.isEscape())) return;
                    //* *
                    //* *
                    //* *VALUE 'J'
                    //* *  MOVE 'TBL' TO #COMMAND-PREV
                    //* *  FETCH 'FSFJ0NOR'
                    //* *
                    //* *
                }                                                                                                                                                         //Natural: VALUE 'K'
                else if (condition((gdaFsgsys.getPnd_Command().equals("K"))))
                {
                    decideConditionsMet263++;
                    //*  DK 3/00
                    gdaFsgsys.getPnd_Command_Prev().setValue("TBL");                                                                                                      //Natural: MOVE 'TBL' TO #COMMAND-PREV
                    //*  DK 3/00
                    Global.setFetchProgram(DbsUtil.getBlType("FSCD0NOR"));                                                                                                //Natural: FETCH 'FSCD0NOR'
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    Map.reinput(new ReinputWithText("INVALID COMMAND, REENTER", new FieldAttributes("CD=YE")), new ReinputAlarm());                                       //Natural: REINPUT ( CD = YE ) 'INVALID COMMAND, REENTER' AND SOUND ALARM
                }                                                                                                                                                         //Natural: END-DECIDE
                //* *
                //* *
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
