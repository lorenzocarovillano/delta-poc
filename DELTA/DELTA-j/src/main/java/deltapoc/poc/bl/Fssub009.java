/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-01-15 07:03:40 PM
**        * FROM NATURAL SUBPROGRAM : Fssub009
************************************************************
**        * FILE NAME            : Fssub009.java
**        * CLASS NAME           : Fssub009
**        * INSTANCE NAME        : Fssub009
************************************************************
************************************************************************
* PROGRAM  : FSSUB009
* SYSTEM   : SOFRS
* TITLE    : RETURNS TO THE CALLING PROGRAM THE VALID FLIGHT-TYPE FOR
*            THE FLIGHT-NUMBER AND DATE SUPPLIED VIA THE PARAMETER
*            AREA.
* FUNCTION : THIS PROGRAM READS THE FILE FLIGHT-NUMBER-XREF FOR THE
*            FLIGHT-NUMBER AND DATE PASSED IN THE PDA
*            FSSUB09A.#INPUTS ; A MESSAGE IS SENT BACK IF THE DATE
*            OR NO FLIGHT-TYPE IS FOUND.
*
*            IT WILL FIND A FLIGHT-TYPE IF THERE IS AN ACTIVE RECORD
*            IN FLIGHT-NUMBER-XREF, FOR THE FLIGHT NUMBER SUPPLIED
*            AND THE DATE SUPPLIED FALLS IN THE RANGE OF THE FILE
*            EFFECTIVE-START-DATE AND EFFECTIVE-END-DATE.
*
*            TWO TYPES OF ERROR MESSAGES COULD BE PASSED TO THE CALLING
*            PROGRAM. ONE IS CRITICAL (ERR-TYP = 'C') WHICH MEANS THE
*            CALLING PROGRAM SHOULD IMMEDIATELY TERMINATE WITH THE
*            MESSAGE. THE OTHER IS WARNING WHICH MEANS AN ERROR WAS
*            FOUND AND THE ACTION TO TAKE DEPENDS ON THE CALLING
*            PROGRAM.
*
* HISTORY
* CREATED ON 08/29/96 BY EDUARDO M BRITES (TRIAD DATA INC).
*
* MODIFIED ON 05/26/98 BY ANA KENNEDY (METRO INFORMATION SERVICES)
*   - Y2K
*
************************************************************************

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fssub009 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFssub09a pdaFssub09a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_P_Err_Msg;
    private DbsField pnd_P_Err_Type;
    private DbsField pnd_Display_Date;

    private DbsGroup pnd_Eff_Dtes;
    private DbsField pnd_Eff_Dtes_Pnd_Flight_Date;
    private DbsField pnd_Found;
    private DbsField pnd_Sp1;

    private DbsGroup pnd_Sp1__R_Field_1;

    private DbsGroup pnd_Sp1_Pnd_Sp1_Struct;
    private DbsField pnd_Sp1_Flight_Number;
    private DbsField pnd_Sp1_Effective_End_Date;
    private DbsField pnd_Text;
    private DbsField pnd_Wrk_Date;

    private DbsGroup pnd_Wrk_Date__R_Field_2;

    private DbsGroup pnd_Wrk_Date_Pnd_Wrk_Date_Struct;
    private DbsField pnd_Wrk_Date_Pnd_Cc;
    private DbsField pnd_Wrk_Date_Pnd_Yy;

    private DataAccessProgramView vw_flt_Xref;
    private DbsField flt_Xref_Flight_Number;
    private DbsField flt_Xref_Flight_Type;

    private DbsGroup flt_Xref_Effective_Period;
    private DbsField flt_Xref_Effective_Start_Date;
    private DbsField flt_Xref_Effective_End_Date;
    private DbsField flt_Xref_Active_Tag;

    private void initializeFields() throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFssub09a = new PdaFssub09a(parameters);
        pnd_P_Err_Msg = parameters.newFieldInRecord("pnd_P_Err_Msg", "#P-ERR-MSG", FieldType.STRING, 71);
        pnd_P_Err_Msg.setParameterOption(ParameterOption.ByReference);
        pnd_P_Err_Type = parameters.newFieldInRecord("pnd_P_Err_Type", "#P-ERR-TYPE", FieldType.STRING, 1);
        pnd_P_Err_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Display_Date = localVariables.newFieldInRecord("pnd_Display_Date", "#DISPLAY-DATE", FieldType.STRING, 10);

        pnd_Eff_Dtes = localVariables.newGroupInRecord("pnd_Eff_Dtes", "#EFF-DTES");
        pnd_Eff_Dtes_Pnd_Flight_Date = pnd_Eff_Dtes.newFieldInGroup("pnd_Eff_Dtes_Pnd_Flight_Date", "#FLIGHT-DATE", FieldType.DATE);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Sp1 = localVariables.newFieldInRecord("pnd_Sp1", "#SP1", FieldType.STRING, 10);

        pnd_Sp1__R_Field_1 = localVariables.newGroupInRecord("pnd_Sp1__R_Field_1", "REDEFINE", pnd_Sp1);

        pnd_Sp1_Pnd_Sp1_Struct = pnd_Sp1__R_Field_1.newGroupInGroup("pnd_Sp1_Pnd_Sp1_Struct", "#SP1-STRUCT");
        pnd_Sp1_Flight_Number = pnd_Sp1_Pnd_Sp1_Struct.newFieldInGroup("pnd_Sp1_Flight_Number", "FLIGHT-NUMBER", FieldType.STRING, 5);
        pnd_Sp1_Effective_End_Date = pnd_Sp1_Pnd_Sp1_Struct.newFieldInGroup("pnd_Sp1_Effective_End_Date", "EFFECTIVE-END-DATE", FieldType.DATE);
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 15);
        pnd_Wrk_Date = localVariables.newFieldInRecord("pnd_Wrk_Date", "#WRK-DATE", FieldType.STRING, 8);

        pnd_Wrk_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Wrk_Date__R_Field_2", "REDEFINE", pnd_Wrk_Date);

        pnd_Wrk_Date_Pnd_Wrk_Date_Struct = pnd_Wrk_Date__R_Field_2.newGroupInGroup("pnd_Wrk_Date_Pnd_Wrk_Date_Struct", "#WRK-DATE-STRUCT");
        pnd_Wrk_Date_Pnd_Cc = pnd_Wrk_Date_Pnd_Wrk_Date_Struct.newFieldInGroup("pnd_Wrk_Date_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Wrk_Date_Pnd_Yy = pnd_Wrk_Date_Pnd_Wrk_Date_Struct.newFieldInGroup("pnd_Wrk_Date_Pnd_Yy", "#YY", FieldType.STRING, 2);

        vw_flt_Xref = new DataAccessProgramView(new NameInfo("vw_flt_Xref", "FLT-XREF"), "FLIGHT_NUMBER_XREF", "FLIGHT_NUMBR_XRF");
        flt_Xref_Flight_Number = vw_flt_Xref.getRecord().newFieldInGroup("flt_Xref_Flight_Number", "FLIGHT-NUMBER", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "FLIGHT_NUMBER");
        flt_Xref_Flight_Type = vw_flt_Xref.getRecord().newFieldInGroup("flt_Xref_Flight_Type", "FLIGHT-TYPE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "FLIGHT_TYPE");

        flt_Xref_Effective_Period = vw_flt_Xref.getRecord().newGroupInGroup("FLT_XREF_EFFECTIVE_PERIOD", "EFFECTIVE-PERIOD");
        flt_Xref_Effective_Start_Date = flt_Xref_Effective_Period.newFieldInGroup("flt_Xref_Effective_Start_Date", "EFFECTIVE-START-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "EFFECTIVE_START_DATE");
        flt_Xref_Effective_End_Date = flt_Xref_Effective_Period.newFieldInGroup("flt_Xref_Effective_End_Date", "EFFECTIVE-END-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "EFFECTIVE_END_DATE");
        flt_Xref_Active_Tag = vw_flt_Xref.getRecord().newFieldInGroup("flt_Xref_Active_Tag", "ACTIVE-TAG", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_TAG");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_flt_Xref.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Sp1.setInitialValue("H'00000000000000000000'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fssub009() throws Exception
    {
        super("Fssub009");
        initializeFields();
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFssub09a.getFssub09a_Pnd_Outputs().reset();                                                                                                                    //Natural: RESET FSSUB09A.#OUTPUTS #P-ERR-MSG #P-ERR-TYPE
        pnd_P_Err_Msg.reset();
        pnd_P_Err_Type.reset();
        pnd_Sp1_Pnd_Sp1_Struct.setValuesByName(pdaFssub09a.getFssub09a_Pnd_Inputs());                                                                                     //Natural: MOVE BY NAME FSSUB09A.#INPUTS TO #SP1-STRUCT
        pnd_Sp1_Flight_Number.setValue(pnd_Sp1_Flight_Number, MoveOption.RightJustified);                                                                                 //Natural: MOVE RIGHT #SP1.FLIGHT-NUMBER TO #SP1.FLIGHT-NUMBER
        DbsUtil.examine(new ExamineSource(pnd_Sp1_Flight_Number,true), new ExamineSearch(" "), new ExamineReplace("0"));                                                  //Natural: EXAMINE FULL #SP1.FLIGHT-NUMBER ' ' REPLACE WITH '0'
        pnd_Wrk_Date.setValue(pdaFssub09a.getFssub09a_Pnd_Flight_Date());                                                                                                 //Natural: ASSIGN #WRK-DATE = FSSUB09A.#FLIGHT-DATE
        //*  USED FOR ERR MSG.
        pnd_Display_Date.setValue(pdaFssub09a.getFssub09a_Pnd_Flight_Date());                                                                                             //Natural: ASSIGN #DISPLAY-DATE = FSSUB09A.#FLIGHT-DATE
        pnd_Text.setValue("FLIGHT DATE");                                                                                                                                 //Natural: ASSIGN #TEXT = 'FLIGHT DATE'
                                                                                                                                                                          //Natural: PERFORM SET-WRK-DATE
        sub_Set_Wrk_Date();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_P_Err_Msg.notEquals(" ")))                                                                                                                      //Natural: IF #P-ERR-MSG NE ' '
        {
            pnd_P_Err_Type.setValue("C");                                                                                                                                 //Natural: ASSIGN #P-ERR-TYPE = 'C'
            //*  RETURN TO THE CALLING PROGRAM.
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Eff_Dtes_Pnd_Flight_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wrk_Date);                                                                         //Natural: MOVE EDITED #WRK-DATE TO #EFF-DTES.#FLIGHT-DATE ( EM = YYYYMMDD )
        pnd_Display_Date.setValueEdited(pnd_Eff_Dtes_Pnd_Flight_Date,new ReportEditMask("MM/DD/YYYY"));                                                                   //Natural: MOVE EDITED #EFF-DTES.#FLIGHT-DATE ( EM = MM/DD/YYYY ) TO #DISPLAY-DATE
        //*  NOW THAT THE DATE IS VALID STORE IT BACK IN THE SUPPLIED
        //*  FIELD SO THAT THE CALLING PROGRAM WILL HAVE THE CORRECT FORMAT :
        pdaFssub09a.getFssub09a_Pnd_Flight_Date().setValueEdited(pnd_Eff_Dtes_Pnd_Flight_Date,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED #EFF-DTES.#FLIGHT-DATE ( EM = YYYYMMDD ) TO FSSUB09A.#FLIGHT-DATE
        pdaFssub09a.getFssub09a_Flight_Number().setValue(pnd_Sp1_Flight_Number);                                                                                          //Natural: ASSIGN FSSUB09A.FLIGHT-NUMBER = #SP1.FLIGHT-NUMBER
        //*  STARTING SEARCHING FOR RECORDS WITH AN END DATE EQUAL TO OR GREATER
        //*  THAN THE START DATE SUPPLIED VIA THE PARMS :
        pnd_Sp1_Effective_End_Date.setValue(pnd_Eff_Dtes_Pnd_Flight_Date);                                                                                                //Natural: ASSIGN #SP1.EFFECTIVE-END-DATE = #EFF-DTES.#FLIGHT-DATE
        vw_flt_Xref.startDatabaseRead                                                                                                                                     //Natural: READ FLT-XREF WITH FLT-NBR/END-DATE/ACT = #SP1
        (
        "MAINRD1",
        new Wc[] { new Wc("FLT_NBR_END_DATE_ACT", ">=", pnd_Sp1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("FLT_NBR_END_DATE_ACT", "ASC") }
        );
        MAINRD1:
        while (condition(vw_flt_Xref.readNextRow("MAINRD1")))
        {
            if (condition(flt_Xref_Flight_Number.notEquals(pnd_Sp1_Flight_Number)))                                                                                       //Natural: IF FLT-XREF.FLIGHT-NUMBER NE #SP1.FLIGHT-NUMBER
            {
                if (true) break MAINRD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( MAINRD1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  ACTIVE RECORDS ONLY.
            if (condition(flt_Xref_Active_Tag.notEquals("A")))                                                                                                            //Natural: REJECT IF FLT-XREF.ACTIVE-TAG NE 'A'
            {
                continue;
            }
            if (condition(flt_Xref_Effective_Start_Date.greater(pnd_Eff_Dtes_Pnd_Flight_Date)))                                                                           //Natural: IF FLT-XREF.EFFECTIVE-START-DATE > #EFF-DTES.#FLIGHT-DATE
            {
                if (true) break MAINRD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( MAINRD1. )
            }                                                                                                                                                             //Natural: END-IF
            pdaFssub09a.getFssub09a_Pnd_Outputs().setValuesByName(vw_flt_Xref);                                                                                           //Natural: MOVE BY NAME FLT-XREF TO FSSUB09A.#OUTPUTS
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND = TRUE
            if (true) break MAINRD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( MAINRD1. )
            //*  (MAINRD1.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            pnd_P_Err_Type.setValue("W");                                                                                                                                 //Natural: ASSIGN #P-ERR-TYPE = 'W'
            pnd_P_Err_Msg.setValue(DbsUtil.compress("FLIGHT", pdaFssub09a.getFssub09a_Flight_Number(), "FOR DATE", pnd_Display_Date, "NOT FOUND IN FLIGHT NBR XREF FILE.")); //Natural: COMPRESS 'FLIGHT' FSSUB09A.FLIGHT-NUMBER 'FOR DATE' #DISPLAY-DATE 'NOT FOUND IN FLIGHT NBR XREF FILE.' INTO #P-ERR-MSG
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
        //*  SUBROUTINES
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-WRK-DATE
    }
    private void sub_Set_Wrk_Date() throws Exception                                                                                                                      //Natural: SET-WRK-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  RIGHT JUSTIFY.
        pnd_Wrk_Date.setValue(pnd_Wrk_Date, MoveOption.RightJustified);                                                                                                   //Natural: MOVE RIGHT #WRK-DATE TO #WRK-DATE
        //*  FEEL LEADING SPACES WITH
        DbsUtil.examine(new ExamineSource(pnd_Wrk_Date,true), new ExamineSearch(" "), new ExamineReplace("0"));                                                           //Natural: EXAMINE FULL #WRK-DATE ' ' REPLACE WITH '0'
        //*  ZEROS.
        if (condition(pnd_Wrk_Date.notEquals("00000000") && pnd_Wrk_Date_Pnd_Cc.equals("00")))                                                                            //Natural: IF #WRK-DATE NE '00000000' AND #WRK-DATE.#CC = '00'
        {
            //*  Y2K
            if (condition(pnd_Wrk_Date_Pnd_Yy.lessOrEqual("55")))                                                                                                         //Natural: IF #WRK-DATE.#YY LE '55'
            {
                pnd_Wrk_Date_Pnd_Cc.setValue("20");                                                                                                                       //Natural: ASSIGN #WRK-DATE.#CC = '20'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wrk_Date_Pnd_Cc.setValue("19");                                                                                                                       //Natural: ASSIGN #WRK-DATE.#CC = '19'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(pnd_Wrk_Date,"YYYYMMDD"))))                                                                                                  //Natural: IF #WRK-DATE NE MASK ( YYYYMMDD )
        {
            pnd_P_Err_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INVALID DATE=", pnd_Display_Date, "H'00'", "FOR FLT", "H'00'", pdaFssub09a.getFssub09a_Flight_Number(),  //Natural: COMPRESS 'INVALID DATE=' #DISPLAY-DATE H'00' 'FOR FLT' H'00' FSSUB09A.FLIGHT-NUMBER H'00' 'PASSED TO' H'00' *PROGRAM '; CALL SOFRS TEAM.' INTO #P-ERR-MSG LEAVING NO SPACE
                "H'00'", "PASSED TO", "H'00'", Global.getPROGRAM(), "; CALL SOFRS TEAM."));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET-WRK-DATE
    }

    //
}
