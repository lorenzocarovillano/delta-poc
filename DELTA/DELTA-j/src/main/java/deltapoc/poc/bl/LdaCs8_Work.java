/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:39 PM
**        * FROM NATURAL LDA     : CS8_WORK
************************************************************
**        * FILE NAME            : LdaCs8_Work.java
**        * CLASS NAME           : LdaCs8_Work
**        * INSTANCE NAME        : LdaCs8_Work
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.domain.*;

public final class LdaCs8_Work extends DbsRecord
{
    // Properties
    private DbsField pnd_Cs8_Work;
    private DbsGroup pnd_Cs8_WorkRedef1;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Act;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Ret_Code;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Date_Prt;
    private DbsGroup pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Day;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Month;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Year;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Day_Of_Year;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Leap_Year;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Mon_Nbr;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Pars_Day;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Pars_Dec;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Days_In_Mth;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Week_Day_Nbr;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Week_Day;
    private DbsField pnd_Cs8_Work_Pnd_Cs8_Century;

    public DbsField getPnd_Cs8_Work() { return pnd_Cs8_Work; }

    public DbsGroup getPnd_Cs8_WorkRedef1() { return pnd_Cs8_WorkRedef1; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Act() { return pnd_Cs8_Work_Pnd_Cs8_Act; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Ret_Code() { return pnd_Cs8_Work_Pnd_Cs8_Ret_Code; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Date_Prt() { return pnd_Cs8_Work_Pnd_Cs8_Date_Prt; }

    public DbsGroup getPnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2() { return pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Day() { return pnd_Cs8_Work_Pnd_Cs8_Day; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Month() { return pnd_Cs8_Work_Pnd_Cs8_Month; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Year() { return pnd_Cs8_Work_Pnd_Cs8_Year; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Day_Of_Year() { return pnd_Cs8_Work_Pnd_Cs8_Day_Of_Year; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Leap_Year() { return pnd_Cs8_Work_Pnd_Cs8_Leap_Year; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Mon_Nbr() { return pnd_Cs8_Work_Pnd_Cs8_Mon_Nbr; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Pars_Day() { return pnd_Cs8_Work_Pnd_Cs8_Pars_Day; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Pars_Dec() { return pnd_Cs8_Work_Pnd_Cs8_Pars_Dec; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Days_In_Mth() { return pnd_Cs8_Work_Pnd_Cs8_Days_In_Mth; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Week_Day_Nbr() { return pnd_Cs8_Work_Pnd_Cs8_Week_Day_Nbr; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Week_Day() { return pnd_Cs8_Work_Pnd_Cs8_Week_Day; }

    public DbsField getPnd_Cs8_Work_Pnd_Cs8_Century() { return pnd_Cs8_Work_Pnd_Cs8_Century; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cs8_Work = newFieldInRecord("pnd_Cs8_Work", "#CS8-WORK", FieldType.STRING, 80);
        pnd_Cs8_WorkRedef1 = newGroupInRecord("pnd_Cs8_WorkRedef1", "Redefines", pnd_Cs8_Work);
        pnd_Cs8_Work_Pnd_Cs8_Act = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Act", "#CS8-ACT", FieldType.STRING, 3);
        pnd_Cs8_Work_Pnd_Cs8_Ret_Code = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Ret_Code", "#CS8-RET-CODE", FieldType.STRING, 1);
        pnd_Cs8_Work_Pnd_Cs8_Date_Prt = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Date_Prt", "#CS8-DATE-PRT", FieldType.STRING, 7);
        pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2 = pnd_Cs8_WorkRedef1.newGroupInGroup("pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2", "Redefines", pnd_Cs8_Work_Pnd_Cs8_Date_Prt);
        pnd_Cs8_Work_Pnd_Cs8_Day = pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Day", "#CS8-DAY", FieldType.NUMERIC, 2);
        pnd_Cs8_Work_Pnd_Cs8_Month = pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Month", "#CS8-MONTH", FieldType.STRING, 
            3);
        pnd_Cs8_Work_Pnd_Cs8_Year = pnd_Cs8_Work_Pnd_Cs8_Date_PrtRedef2.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Year", "#CS8-YEAR", FieldType.NUMERIC, 2);
        pnd_Cs8_Work_Pnd_Cs8_Day_Of_Year = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Day_Of_Year", "#CS8-DAY-OF-YEAR", FieldType.NUMERIC, 
            3);
        pnd_Cs8_Work_Pnd_Cs8_Leap_Year = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Leap_Year", "#CS8-LEAP-YEAR", FieldType.STRING, 1);
        pnd_Cs8_Work_Pnd_Cs8_Mon_Nbr = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Mon_Nbr", "#CS8-MON-NBR", FieldType.NUMERIC, 2);
        pnd_Cs8_Work_Pnd_Cs8_Pars_Day = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Pars_Day", "#CS8-PARS-DAY", FieldType.BINARY, 2);
        pnd_Cs8_Work_Pnd_Cs8_Pars_Dec = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Pars_Dec", "#CS8-PARS-DEC", FieldType.NUMERIC, 5);
        pnd_Cs8_Work_Pnd_Cs8_Days_In_Mth = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Days_In_Mth", "#CS8-DAYS-IN-MTH", FieldType.NUMERIC, 
            2);
        pnd_Cs8_Work_Pnd_Cs8_Week_Day_Nbr = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Week_Day_Nbr", "#CS8-WEEK-DAY-NBR", FieldType.BINARY, 
            1);
        pnd_Cs8_Work_Pnd_Cs8_Week_Day = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Week_Day", "#CS8-WEEK-DAY", FieldType.STRING, 9);
        pnd_Cs8_Work_Pnd_Cs8_Century = pnd_Cs8_WorkRedef1.newFieldInGroup("pnd_Cs8_Work_Pnd_Cs8_Century", "#CS8-CENTURY", FieldType.NUMERIC, 2);

        this.setRecordName("LdaCs8_Work");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCs8_Work() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
