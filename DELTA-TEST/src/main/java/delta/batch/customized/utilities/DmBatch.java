package delta.batch.customized.utilities;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.config.App;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.google.inject.Inject;
import com.modernsystems.batch.files.IRecordAccessProvider;
import com.modernsystems.batch.files.RecordIos;
import com.modernsystems.ctu.core.impl.BatchProgram;

import bphx.batch.batchlets.Jobs;
import bphx.batch.batchlets.impl.DeleteProcessor;
import bphx.batch.context.IContextProvider;
import bphx.batch.context.JobInfo;
import bphx.batch.step.DDCardInfo;

public class DmBatch extends BatchProgram {

	private static final Logger LOGGER = LoggerFactory.getLogger(DmBatch.class);
	@Inject
	DeleteProcessor dp;
	@Inject
	private IRecordAccessProvider rap;
	@Inject
	protected IContextProvider cp;

	private Charset cs = App.context().getMarshalCharset();

	private static final int SCRATCH_LINE_SIZE = 72;

	public long execute() {

		long ret = 0;
		
		JobInfo ji = Jobs.get().getJobInfo(ret);
		String jn = ji.getJobName();
		
		LOGGER.info("DMBATCH Utility invoked for job {}", jn);

		DisplayUtil.sysout.write("****  WARNING  ****  WARNING  *****  WARNING  ****   ", jn,
				" UNDER DEVELOP AT MOMENT RETURN ALWAYS RC=0    ****");
		DDCardInfo sysinDdCard = cp.getDdInfo().getDDCardInfo("SYSIN");
		if (sysinDdCard != null) {
			try (Reader r = RecordIos.toReader(rap.getReader(sysinDdCard), cs)) {
				List<String> lines = IOUtils.readLines(r);
				LOGGER.info("***** START PRINT SYSIN *****");
				for (String l : lines) {
					int len = Math.min(l.length(), SCRATCH_LINE_SIZE);
					String sysinLine = StringUtils.substring(l, 0, len).trim();
					LOGGER.info("SYSIN:", sysinLine);
				}
				LOGGER.info("***** END PRINT SYSIN *****");
			} catch (IOException e) {
				log.error("Can't read from file: {}!", sysinDdCard.getRecordInfo().getAbsoluteFile(), e);
			}
		}
		Session.setReturnCode(0);
		return 0;
	}

	public static DmBatch getInstance() {
		return new DmBatch();
	}
}
