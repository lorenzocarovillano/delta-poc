package delta.batch.customized.utilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;

import com.bphx.ctu.af.core.config.App;
import com.modernsystems.batch.files.IRecordAccessProvider;
import com.modernsystems.batch.files.IRecordReader;
import com.modernsystems.batch.files.RecordIos;
import com.modernsystems.ctu.core.InjectorProvider;

import ateras.framework.io.springBatch.SbBatchDriver;
import bphx.batch.batchlets.ReturnCode;
import bphx.batch.batchlets.StandardBatchlet;
import bphx.batch.step.DDCardInfo;
 
public class NaturalProgramBatchlet extends StandardBatchlet
{
    private static final String WORKFILE = "CMWKF[0-9]+";
    private static final String PRINTFILE = "CMPRT[0-9]+";
    private static final String CMSYNIN = "CMSYNIN";
    private static final String CMPRINT = "CMPRINT";
    private IRecordAccessProvider rap;
    
    public NaturalProgramBatchlet() {
        this.rap = (IRecordAccessProvider)InjectorProvider.get((Class)IRecordAccessProvider.class);
    }
    
    @Override
	public ReturnCode doProcess() throws Exception {
        return this.executeProgram(this.getProgram(), new String[] { this.getParam() });
    }
    
    private ReturnCode executeProgram(final String programName, final String[] params) {
        try {
            System.out.println("Running NATURAL Batchlet with library: " + programName);
            System.out.println("Found properties: ");
            final Map<String, String> naturalProps = this.filterStepProps();
            naturalProps.keySet().forEach(p -> System.out.println("\t" + p + " -> " + naturalProps.get(p)));
            System.out.println("Using PARAMS " + String.join(", ", params));
            final Map<String, DDCardInfo> workfiles = this.getDdList("CMWKF[0-9]+");
            System.out.println("Found " + workfiles.keySet().size() + " workfile(s): " + String.join(",", workfiles.keySet()));
            final Map<String, DDCardInfo> printfiles = this.getDdList("CMPRT[0-9]+");
            System.out.println("Found " + printfiles.keySet().size() + " printfile(s): " + String.join(",", printfiles.keySet()));
            final List<String> pgmLines = this.readCmsynin().stream().filter(l -> !this.isNaturalComment(l)).collect(Collectors.toList());//(Collector<? super Object, ?, List<String>>)
            int ret = 0;
            for (final String pgm : pgmLines) {
                if (pgm != null && !pgm.equalsIgnoreCase("FIN")) {
                    System.out.println("Found NATURAL program call: " + pgm);
                    final String[] pgmArgs = new String[this.ddCards.size() + 1];
                    pgmArgs[0] = pgm;
                    int i = 1;
                    for (final DDCardInfo dci : this.ddCards.values()) {
                        pgmArgs[i] = String.valueOf(dci.getDdName()) + "=" + dci.getRecordInfo().getAbsoluteFile();
                        ++i;
                    }
                    ret = SbBatchDriver.main(pgmArgs); 
                }
            }
            return new ReturnCode(ret, false);
        }
        catch (Exception e) {
            NaturalProgramBatchlet.log.error("Can't execute program {}!", programName, e);
            return ReturnCode.ABEND;
        }
    }
    
    private Map<String, String> filterStepProps() {
        final Map<String, String> naturalProps = new HashMap<>();
        final Map<String, String> map = new HashMap<>();
        this.getStepProps().keySet().forEach(p -> {
            if (!this.ddCards.containsKey(p) && !"STEP_NAME".equals(p) && !"EXEC".equals(p) && !"arguments".equals(p) && !String.valueOf(p).startsWith("CMSYNIN")) {
                map.put(String.valueOf(p), (String)this.getStepProps().get(p));
            }
            return;
        });
        return naturalProps;
    }
    
    private boolean isNaturalComment(final String line) {
        return line.startsWith("*") || line.startsWith("/*");
    }
    
    protected Map<String, DDCardInfo> getDdList(final String filePattern) {
        return this.ddCards.keySet().stream().filter(ddName -> ddName.matches(filePattern)).collect(Collectors.toMap(Function.identity(), this.ddCards::get));
        
        
    }
    
    public List<String> readCmsynin() {
        try {
            Throwable t = null;
            try {
                final IRecordReader rr = this.rap.getReader(this.ddCards.get("CMSYNIN"));
                try {
                    if (rr != null) {
                        return IOUtils.readLines(RecordIos.toReader(rr, App.context().getMarshalCharset()));
                    }
                    return null;
                }
                finally {
                    if (rr != null) {
                        rr.close();
                    }
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception;
//                    t = exception;
                }
                else {
                    final Throwable exception;
//                    if (t != exception) {
//                        t.addSuppressed(exception);
//                    }
                }
            }
        }
        catch (Exception e) {
            NaturalProgramBatchlet.log.error("Can't read CMSYNIN!", e);
        }
        return null;
    }
}
