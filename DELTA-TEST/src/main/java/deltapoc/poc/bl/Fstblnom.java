/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-01-15 07:03:39 PM
**        *   FROM NATURAL MAP   :  Fstblnom
************************************************************
**        * FILE NAME               : Fstblnom.java
**        * CLASS NAME              : Fstblnom
**        * INSTANCE NAME           : Fstblnom
************************************************************
* MAP2: PROTOTYPE              * INPUT USING MAP 'XXXXXXXX'   *     #AIRLINE-CODE #CC-AIRLINE-NAME #COMMAND #PROGRAM
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Fstblnom extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Airline_Code;
    private DbsField pnd_Cc_Airline_Name;
    private DbsField pnd_Command;
    private DbsField pnd_Program;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Airline_Code = parameters.newFieldInRecord("pnd_Airline_Code", "#AIRLINE-CODE", FieldType.STRING, 2);
        pnd_Cc_Airline_Name = parameters.newFieldInRecord("pnd_Cc_Airline_Name", "#CC-AIRLINE-NAME", FieldType.STRING, 25);
        pnd_Command = parameters.newFieldInRecord("pnd_Command", "#COMMAND", FieldType.STRING, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        parameters.reset();
    }

    public Fstblnom() throws Exception
    {
        super("Fstblnom");
        initializeFields();
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=023 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fstblnom", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("astUSER", Global.getUSER(), true, 1, 2, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 1, 16, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "S O F R S", "", 1, 36, 9);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 58, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("astTIMX", Global.getTIMX(), true, 1, 72, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "****************************CONNECTION CARRIER*********************************", "", 3, 1, 79);
            uiForm.setUiControl("pnd_Airline_Code", pnd_Airline_Code, true, 4, 29, 2, "", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "-", "", 4, 32, 1);
            uiForm.setUiControl("pnd_Cc_Airline_Name", pnd_Cc_Airline_Name, true, 4, 34, 25, "", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "STATISTICS ONLINE FLIGHT REPORTING SYSTEM", "YELLOW", 5, 20, 41);
            uiForm.setUiLabel("label_5", "TABLE DISPLAYS MENU", "YELLOW", 6, 31, 19);
            uiForm.setUiLabel("label_6", "****************************CONNECTION CARRIER*********************************", "", 8, 1, 79);
            uiForm.setUiLabel("label_7", "A - EQUIPMENT", "", 11, 24, 13);
            uiForm.setUiLabel("label_8", "B - FLIGHT SCHEDULE", "", 12, 24, 19);
            uiForm.setUiLabel("label_9", "D - REPORT CODES", "", 13, 24, 16);
            uiForm.setUiLabel("label_10", "E - REGION CODES", "", 14, 24, 16);
            uiForm.setUiLabel("label_11", "G - AIRPORT CODES", "", 15, 24, 17);
            uiForm.setUiLabel("label_12", "H - SHIP CAPACITY", "", 16, 24, 17);
            uiForm.setUiLabel("label_13", "I - FLIGHT NUMBERS", "", 17, 24, 18);
            uiForm.setUiLabel("label_14", "K - CONNECTION CARRIERS", "", 19, 24, 23);
            uiForm.setUiLabel("label_15", "COMMAND===>", "", 23, 2, 11);
            uiForm.setUiControl("pnd_Command", pnd_Command, false, 23, 15, 5, "", true, false, null, null, "AD=D?MFHT' '~TG=", ' ', "FSSD2NOI");
            uiForm.setUiLabel("label_16", "PF1=HELP", "", 23, 24, 8);
            uiForm.setUiLabel("label_17", "PF2=PREVIOUS SCREEN", "", 23, 36, 19);
            uiForm.setUiLabel("label_18", "PF3=EXIT", "", 23, 59, 8);
            uiForm.setUiLabel("label_19", "PF8=PRINT", "", 23, 71, 9);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
