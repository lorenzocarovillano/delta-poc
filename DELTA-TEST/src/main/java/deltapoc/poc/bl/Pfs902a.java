/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210126
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-28 08:01:27 AM
**        * FROM NATURAL PROGRAM : Pfs902a
************************************************************
**        * FILE NAME            : Pfs902a.java
**        * CLASS NAME           : Pfs902a
**        * INSTANCE NAME        : Pfs902a
************************************************************

************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;

public class Pfs902a extends BLNatBase
{
    // Data Areas
    private LdaCs8_Work ldaCs8_Work;
    private PdaFssub09a pdaFssub09a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnt;
    private DbsField pnd_End_Date;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Err_Typ;
    private DbsField pnd_First;
    private DbsField pnd_Flt_Date;

    private DbsGroup pnd_Flt_Date__R_Field_1;
    private DbsField pnd_Flt_Date_Pnd_Flt_Cc;
    private DbsField pnd_Flt_Date_Pnd_Flt_Yy;
    private DbsField pnd_Flt_Date_Pnd_Flt_Mm;
    private DbsField pnd_Flt_Date_Pnd_Flt_Dd;
    private DbsField pnd_Prt_Date;

    private DbsGroup pnd_Prt_Date__R_Field_2;
    private DbsField pnd_Prt_Date_Pnd_P_Mm;
    private DbsField pnd_Prt_Date_Pnd_P_Sl1;
    private DbsField pnd_Prt_Date_Pnd_P_Dd;
    private DbsField pnd_Prt_Date_Pnd_P_Sl2;
    private DbsField pnd_Prt_Date_Pnd_P_Yy;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Super;

    private DbsGroup pnd_Super__R_Field_3;
    private DbsField pnd_Super_Pnd_Sup_Date;
    private DbsField pnd_Super_Pnd_Sup_Flt;
    private DbsField pnd_Super_Pnd_Sup_Leg;
    private DbsField pnd_Title_Date1;

    private DbsGroup pnd_Title_Date1__R_Field_4;
    private DbsField pnd_Title_Date1_Pnd_T_Mm1;
    private DbsField pnd_Title_Date1_Pnd_T_Sl1a;
    private DbsField pnd_Title_Date1_Pnd_T_Dd1;
    private DbsField pnd_Title_Date1_Pnd_T_Sl1b;
    private DbsField pnd_Title_Date1_Pnd_T_Yy1;
    private DbsField pnd_Title_Date2;

    private DbsGroup pnd_Title_Date2__R_Field_5;
    private DbsField pnd_Title_Date2_Pnd_T_Mm2;
    private DbsField pnd_Title_Date2_Pnd_T_Sl2a;
    private DbsField pnd_Title_Date2_Pnd_T_Dd2;
    private DbsField pnd_Title_Date2_Pnd_T_Sl2b;
    private DbsField pnd_Title_Date2_Pnd_T_Yy2;

    private DataAccessProgramView vw_regions;
    private DbsField regions_Station_1;
    private DbsField regions_Station_2;

    private DataAccessProgramView vw_schedule;
    private DbsField schedule_Date;
    private DbsField schedule_Destination;
    private DbsField schedule_Flight_Number;
    private DbsField schedule_Origin;

    //Data Initialization Methods
    private void initializeFields() throws Exception
    {
        //Data Areas
        ldaCs8_Work = new LdaCs8_Work();
        registerRecord(ldaCs8_Work);
        localVariables = new DbsRecord();
        pdaFssub09a = new PdaFssub09a(localVariables);

        // Local Variables
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 5);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.BINARY, 2);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 71);
        pnd_Err_Typ = localVariables.newFieldInRecord("pnd_Err_Typ", "#ERR-TYP", FieldType.STRING, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.STRING, 1);
        pnd_Flt_Date = localVariables.newFieldInRecord("pnd_Flt_Date", "#FLT-DATE", FieldType.STRING, 8);

        pnd_Flt_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Flt_Date__R_Field_1", "REDEFINE", pnd_Flt_Date);
        pnd_Flt_Date_Pnd_Flt_Cc = pnd_Flt_Date__R_Field_1.newFieldInGroup("pnd_Flt_Date_Pnd_Flt_Cc", "#FLT-CC", FieldType.NUMERIC, 2);
        pnd_Flt_Date_Pnd_Flt_Yy = pnd_Flt_Date__R_Field_1.newFieldInGroup("pnd_Flt_Date_Pnd_Flt_Yy", "#FLT-YY", FieldType.NUMERIC, 2);
        pnd_Flt_Date_Pnd_Flt_Mm = pnd_Flt_Date__R_Field_1.newFieldInGroup("pnd_Flt_Date_Pnd_Flt_Mm", "#FLT-MM", FieldType.NUMERIC, 2);
        pnd_Flt_Date_Pnd_Flt_Dd = pnd_Flt_Date__R_Field_1.newFieldInGroup("pnd_Flt_Date_Pnd_Flt_Dd", "#FLT-DD", FieldType.NUMERIC, 2);
        pnd_Prt_Date = localVariables.newFieldInRecord("pnd_Prt_Date", "#PRT-DATE", FieldType.STRING, 8);

        pnd_Prt_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Prt_Date__R_Field_2", "REDEFINE", pnd_Prt_Date);
        pnd_Prt_Date_Pnd_P_Mm = pnd_Prt_Date__R_Field_2.newFieldInGroup("pnd_Prt_Date_Pnd_P_Mm", "#P-MM", FieldType.NUMERIC, 2);
        pnd_Prt_Date_Pnd_P_Sl1 = pnd_Prt_Date__R_Field_2.newFieldInGroup("pnd_Prt_Date_Pnd_P_Sl1", "#P-SL1", FieldType.STRING, 1);
        pnd_Prt_Date_Pnd_P_Dd = pnd_Prt_Date__R_Field_2.newFieldInGroup("pnd_Prt_Date_Pnd_P_Dd", "#P-DD", FieldType.NUMERIC, 2);
        pnd_Prt_Date_Pnd_P_Sl2 = pnd_Prt_Date__R_Field_2.newFieldInGroup("pnd_Prt_Date_Pnd_P_Sl2", "#P-SL2", FieldType.STRING, 1);
        pnd_Prt_Date_Pnd_P_Yy = pnd_Prt_Date__R_Field_2.newFieldInGroup("pnd_Prt_Date_Pnd_P_Yy", "#P-YY", FieldType.NUMERIC, 2);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.BINARY, 2);
        pnd_Super = localVariables.newFieldInRecord("pnd_Super", "#SUPER", FieldType.STRING, 9);

        pnd_Super__R_Field_3 = localVariables.newGroupInRecord("pnd_Super__R_Field_3", "REDEFINE", pnd_Super);
        pnd_Super_Pnd_Sup_Date = pnd_Super__R_Field_3.newFieldInGroup("pnd_Super_Pnd_Sup_Date", "#SUP-DATE", FieldType.BINARY, 2);
        pnd_Super_Pnd_Sup_Flt = pnd_Super__R_Field_3.newFieldInGroup("pnd_Super_Pnd_Sup_Flt", "#SUP-FLT", FieldType.NUMERIC, 5);
        pnd_Super_Pnd_Sup_Leg = pnd_Super__R_Field_3.newFieldInGroup("pnd_Super_Pnd_Sup_Leg", "#SUP-LEG", FieldType.STRING, 2);
        pnd_Title_Date1 = localVariables.newFieldInRecord("pnd_Title_Date1", "#TITLE-DATE1", FieldType.STRING, 8);

        pnd_Title_Date1__R_Field_4 = localVariables.newGroupInRecord("pnd_Title_Date1__R_Field_4", "REDEFINE", pnd_Title_Date1);
        pnd_Title_Date1_Pnd_T_Mm1 = pnd_Title_Date1__R_Field_4.newFieldInGroup("pnd_Title_Date1_Pnd_T_Mm1", "#T-MM1", FieldType.NUMERIC, 2);
        pnd_Title_Date1_Pnd_T_Sl1a = pnd_Title_Date1__R_Field_4.newFieldInGroup("pnd_Title_Date1_Pnd_T_Sl1a", "#T-SL1A", FieldType.STRING, 1);
        pnd_Title_Date1_Pnd_T_Dd1 = pnd_Title_Date1__R_Field_4.newFieldInGroup("pnd_Title_Date1_Pnd_T_Dd1", "#T-DD1", FieldType.NUMERIC, 2);
        pnd_Title_Date1_Pnd_T_Sl1b = pnd_Title_Date1__R_Field_4.newFieldInGroup("pnd_Title_Date1_Pnd_T_Sl1b", "#T-SL1B", FieldType.STRING, 1);
        pnd_Title_Date1_Pnd_T_Yy1 = pnd_Title_Date1__R_Field_4.newFieldInGroup("pnd_Title_Date1_Pnd_T_Yy1", "#T-YY1", FieldType.NUMERIC, 2);
        pnd_Title_Date2 = localVariables.newFieldInRecord("pnd_Title_Date2", "#TITLE-DATE2", FieldType.STRING, 8);

        pnd_Title_Date2__R_Field_5 = localVariables.newGroupInRecord("pnd_Title_Date2__R_Field_5", "REDEFINE", pnd_Title_Date2);
        pnd_Title_Date2_Pnd_T_Mm2 = pnd_Title_Date2__R_Field_5.newFieldInGroup("pnd_Title_Date2_Pnd_T_Mm2", "#T-MM2", FieldType.NUMERIC, 2);
        pnd_Title_Date2_Pnd_T_Sl2a = pnd_Title_Date2__R_Field_5.newFieldInGroup("pnd_Title_Date2_Pnd_T_Sl2a", "#T-SL2A", FieldType.STRING, 1);
        pnd_Title_Date2_Pnd_T_Dd2 = pnd_Title_Date2__R_Field_5.newFieldInGroup("pnd_Title_Date2_Pnd_T_Dd2", "#T-DD2", FieldType.NUMERIC, 2);
        pnd_Title_Date2_Pnd_T_Sl2b = pnd_Title_Date2__R_Field_5.newFieldInGroup("pnd_Title_Date2_Pnd_T_Sl2b", "#T-SL2B", FieldType.STRING, 1);
        pnd_Title_Date2_Pnd_T_Yy2 = pnd_Title_Date2__R_Field_5.newFieldInGroup("pnd_Title_Date2_Pnd_T_Yy2", "#T-YY2", FieldType.NUMERIC, 2);

        vw_regions = new DataAccessProgramView(new NameInfo("vw_regions", "REGIONS"), "REGION_CODE_MASTER", "REGION_CODE_MST");
        regions_Station_1 = vw_regions.getRecord().newFieldInGroup("regions_Station_1", "STATION-1", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "STATION_1");
        regions_Station_1.setDdmHeader("ORG/CTY");
        regions_Station_2 = vw_regions.getRecord().newFieldInGroup("regions_Station_2", "STATION-2", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "STATION_2");
        regions_Station_2.setDdmHeader("DST/CTY");
        registerRecord(vw_regions);

        vw_schedule = new DataAccessProgramView(new NameInfo("vw_schedule", "SCHEDULE"), "FLIGHT_SCHEDULE_MASTER", "FLT_SKED_MASTER");
        schedule_Date = vw_schedule.getRecord().newFieldInGroup("schedule_Date", "DATE", FieldType.BINARY, 2, RepeatingFieldStrategy.None, "DATE");
        schedule_Date.setDdmHeader("DATE");
        schedule_Destination = vw_schedule.getRecord().newFieldInGroup("schedule_Destination", "DESTINATION", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "DESTINATION");
        schedule_Destination.setDdmHeader("DEST");
        schedule_Flight_Number = vw_schedule.getRecord().newFieldInGroup("schedule_Flight_Number", "FLIGHT-NUMBER", FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, 
            "FLIGHT_NUMBER");
        schedule_Flight_Number.setDdmHeader("FLT/NBR");
        schedule_Origin = vw_schedule.getRecord().newFieldInGroup("schedule_Origin", "ORIGIN", FieldType.STRING, 3, RepeatingFieldStrategy.None, "ORIGIN");
        schedule_Origin.setDdmHeader("ORG");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_regions.reset();
        vw_schedule.reset();

        ldaCs8_Work.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pfs902a() throws Exception
    {
        super("Pfs902a");
        initializeFields();
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *ASSIGN #CS8-ACT = 'SYS'
        //* *CALL 'CS008' #CS8-WORK
        //* *ASSIGN #SUP-DATE = #CS8-PARS-DAY
        pnd_Super_Pnd_Sup_Date.setValue("H'52DF'");                                                                                                                       //Natural: ASSIGN #SUP-DATE = H'52DF'
        //*  ASSIGN #SUP-DATE = H'5299'
        pnd_Title_Date1_Pnd_T_Sl1a.setValue("/");                                                                                                                         //Natural: ASSIGN #T-SL1A = #T-SL1B = #T-SL2A = #T-SL2B = '/'
        pnd_Title_Date1_Pnd_T_Sl1b.setValue("/");
        pnd_Title_Date2_Pnd_T_Sl2a.setValue("/");
        pnd_Title_Date2_Pnd_T_Sl2b.setValue("/");
        //* *ASSIGN #T-MM1 = 01
        pnd_Title_Date1_Pnd_T_Mm1.setValue(ldaCs8_Work.getPnd_Cs8_Work_Pnd_Cs8_Mon_Nbr());                                                                                //Natural: ASSIGN #T-MM1 = #CS8-MON-NBR
        //* *ASSIGN #T-DD1 = #CS8-DAY
        pnd_Title_Date1_Pnd_T_Dd1.setValue(29);                                                                                                                           //Natural: ASSIGN #T-DD1 = 29
        //* *ASSIGN #T-YY1 = #CS8-YEAR
        pnd_Title_Date1_Pnd_T_Yy1.setValue(21);                                                                                                                           //Natural: ASSIGN #T-YY1 = 21
        //* *ADD 60 TO #CS8-PARS-DEC
        //* *ASSIGN #CS8-ACT = 'PRD'
        //* *CALL 'CS008' #CS8-WORK
        //* *ASSIGN #END-DATE = #CS8-PARS-DAY
        pnd_End_Date.setValue("H'52DF'");                                                                                                                                 //Natural: ASSIGN #END-DATE = H'52DF'
        //* *ASSIGN #T-MM2 = #CS8-MON-NBR
        pnd_Title_Date2_Pnd_T_Mm2.setValue(1);                                                                                                                            //Natural: ASSIGN #T-MM2 = 01
        //* *ASSIGN #T-DD2 = #CS8-DAY
        pnd_Title_Date2_Pnd_T_Dd2.setValue(28);                                                                                                                           //Natural: ASSIGN #T-DD2 = 28
        //* *ASSIGN #T-YY2 = #CS8-YEAR
        pnd_Title_Date2_Pnd_T_Yy2.setValue(21);                                                                                                                           //Natural: ASSIGN #T-YY2 = 21
        pnd_First.setValue("Y");                                                                                                                                          //Natural: ASSIGN #FIRST = 'Y'
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 'CSR 3556' 25X 'S O F R S' 20X 'DATE:' *DATU / 'PROGRAM:' *PROGRAM 45X 'PAGE:' *PAGE-NUMBER ( 1 ) // 10X 'FLIGHT SCHEDULE MASTER VERSUS REGION CODE MASTER COMPARISON' 'FOR DL' / 25X 'FROM' #TITLE-DATE1 'THRU' #TITLE-DATE2 // 'MARKETS NOT FOUND IN THE REGION CODE MASTER COMPARISON' // 'ORIG' 'DEST' 'FLIGHT' 3X 'DATE' 3X 'TYPE' / '-' ( 3 ) 2X '-' ( 4 ) '-' ( 6 ) '-' ( 8 ) '-' ( 5 )
        vw_schedule.startDatabaseRead                                                                                                                                     //Natural: READ SCHEDULE BY DATE/FLT-NBR/LEG-SEQ-NBR = #SUPER
        (
        "PND_PND_RD1",
        new Wc[] { new Wc("DATE_FLT_NBR_LEG_SEQ_NBR", ">=", pnd_Super.getBinary(), WcType.BY) },
        new Oc[] { new Oc("DATE_FLT_NBR_LEG_SEQ_NBR", "ASC") }
        );
        PND_PND_RD1:
        while (condition(vw_schedule.readNextRow("PND_PND_RD1")))
        {
            pdaFssub09a.getFssub09a_Flight_Number().setValue(schedule_Flight_Number);                                                                                     //Natural: ASSIGN FSSUB09A.FLIGHT-NUMBER = SCHEDULE.FLIGHT-NUMBER
            //*   ASSIGN #CS8-ACT = 'PRB'
            //*   ASSIGN #CS8-PARS-DAY = DATE
            //*  CALL 'CS008' #CS8-WORK
            //*  ASSIGN #FLT-CC = #CS8-CENTURY
            pnd_Flt_Date_Pnd_Flt_Cc.setValue(20);                                                                                                                         //Natural: ASSIGN #FLT-CC = 20
            //*  ASSIGN #FLT-YY = #CS8-YEAR
            pnd_Flt_Date_Pnd_Flt_Yy.setValue(21);                                                                                                                         //Natural: ASSIGN #FLT-YY = 21
            //*  ASSIGN #FLT-MM = #CS8-MON-NBR
            pnd_Flt_Date_Pnd_Flt_Mm.setValue(1);                                                                                                                          //Natural: ASSIGN #FLT-MM = 01
            //*  ASSIGN #FLT-DD = #CS8-DAY
            pnd_Flt_Date_Pnd_Flt_Dd.setValue(99);                                                                                                                         //Natural: ASSIGN #FLT-DD = 99
            pdaFssub09a.getFssub09a_Pnd_Flight_Date().setValue(pnd_Flt_Date);                                                                                             //Natural: ASSIGN FSSUB09A.#FLIGHT-DATE = #FLT-DATE
            DbsUtil.callnat(Fssub009.class , getCurrentProcessState(), pdaFssub09a.getFssub09a(), pnd_Err_Msg, pnd_Err_Typ);                                              //Natural: CALLNAT 'FSSUB009' FSSUB09A #ERR-MSG #ERR-TYP
            if (condition(Global.isEscape())) return;
            if (condition(pdaFssub09a.getFssub09a_Flight_Type().equals("DLTRG") || pdaFssub09a.getFssub09a_Flight_Type().equals("DLTSF") || pdaFssub09a.getFssub09a_Flight_Type().equals("DLTST")  //Natural: IF FSSUB09A.FLIGHT-TYPE = 'DLTRG' OR = 'DLTSF' OR = 'DLTST' OR = 'DLTSU' THEN
                || pdaFssub09a.getFssub09a_Flight_Type().equals("DLTSU")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  (0990)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(schedule_Date.greater(pnd_End_Date)))                                                                                                           //Natural: IF DATE > #END-DATE THEN
            {
                if (true) break PND_PND_RD1;                                                                                                                              //Natural: ESCAPE BOTTOM ( ##RD1. )
                //*  (1020)
            }                                                                                                                                                             //Natural: END-IF
            vw_regions.startDatabaseFind                                                                                                                                  //Natural: FIND ( 1 ) REGIONS WITH STATION-1 = ORIGIN AND STATION-2 = DESTINATION
            (
            "FIND01",
            new Wc[] { new Wc("STATION_1", "=", schedule_Origin, "And", WcType.WITH) ,
            new Wc("STATION_2", "=", schedule_Destination, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_regions.readNextRow("FIND01", true)))
            {
                vw_regions.setIfNotFoundControlFlag(false);
                if (condition(vw_regions.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORDS FOUND
                {
                    pnd_Prt_Date_Pnd_P_Sl1.setValue("/");                                                                                                                 //Natural: ASSIGN #P-SL1 = #P-SL2 = '/'
                    pnd_Prt_Date_Pnd_P_Sl2.setValue("/");
                    pnd_Prt_Date_Pnd_P_Mm.setValue(pnd_Flt_Date_Pnd_Flt_Mm);                                                                                              //Natural: ASSIGN #P-MM = #FLT-MM
                    pnd_Prt_Date_Pnd_P_Dd.setValue(pnd_Flt_Date_Pnd_Flt_Dd);                                                                                              //Natural: ASSIGN #P-DD = #FLT-DD
                    pnd_Prt_Date_Pnd_P_Yy.setValue(pnd_Flt_Date_Pnd_Flt_Yy);                                                                                              //Natural: ASSIGN #P-YY = #FLT-YY
                    getReports().write(1, schedule_Origin,new ColumnSpacing(3),schedule_Destination,schedule_Flight_Number,pnd_Prt_Date,pdaFssub09a.getFssub09a_Flight_Type()); //Natural: WRITE ( 1 ) ORIGIN 3X DESTINATION SCHEDULE.FLIGHT-NUMBER #PRT-DATE FSSUB09A.FLIGHT-TYPE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT
                    //*  (1050)
                }                                                                                                                                                         //Natural: END-NOREC
                //*  (1040)
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  (0840)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cnt.equals(getZero())))                                                                                                                         //Natural: IF #CNT = 0 THEN
        {
            getReports().write(1, "NO RECORDS FOUND");                                                                                                                    //Natural: WRITE ( 1 ) 'NO RECORDS FOUND'
            if (Global.isEscape()) return;
            //*  (1160)
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"CSR 3556",new ColumnSpacing(25),"S O F R S",new ColumnSpacing(20),"DATE:",Global.getDATU(),"\r\n","PROGRAM:",Global.getPROGRAM(),new 
            ColumnSpacing(45),"PAGE:",getReports().getPageNumberDbs(1),"\r\n","\r\n",new ColumnSpacing(10),"FLIGHT SCHEDULE MASTER VERSUS REGION CODE MASTER COMPARISON","FOR DL","\r\n",new 
            ColumnSpacing(25),"FROM",pnd_Title_Date1,"THRU",pnd_Title_Date2,"\r\n","\r\n","MARKETS NOT FOUND IN THE REGION CODE MASTER COMPARISON","\r\n","\r\n","ORIG","DEST","FLIGHT",new 
            ColumnSpacing(3),"DATE",new ColumnSpacing(3),"TYPE","\r\n","-",new ColumnSpacing(2),"-","-","-","-");
    }
}
