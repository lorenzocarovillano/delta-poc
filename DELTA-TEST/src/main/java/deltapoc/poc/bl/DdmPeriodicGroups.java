/* ************************************************************
   **   Modern Systems INC.  COPYRIGHT 2000-2017
   **   eavATP NATURAL/JAVA SaveAs
   **   Product Version: V5.8 - Build 20210115a
   ************************************************************
   ** MOD ID * DESC                 *   DATE
   ************************************************************
   ** INIT   *  INITIAL VERSION     *  2021-01-15 07:03:41 PM
   ************************************************************
   ************************************************************ */
package deltapoc.poc.bl;
import ateras.framework.domain.*; 
import java.util.*;

public class DdmPeriodicGroups {    
    // Properties
    public DbsRecord peFields;
    private Map<String, Integer> isnDictionary;

    private DbsGroup region_Code_MasterPeGroups;
    private DbsGroup region_Code_MasterPast_City_Pair_Info;
    private DbsGroup region_Code_MasterPast_City_Pair_InfoGroup;
    private DbsField region_Code_MasterPast_City_Pair_InfoCast;
    private DbsGroup region_Code_MasterPast_City_Pair_InfoRedef;
    private DbsField region_Code_MasterPast_City_Pair_InfoBinary;
    private DbsField region_Code_MasterPast_Segment_Miles;
    private DbsField region_Code_MasterPast_Region_Code_1;
    private DbsField region_Code_MasterPast_Dot_Miles;
    private DbsField region_Code_MasterPast_Status_Indicator;
    private DbsField region_Code_MasterPast_Start_Date;
    private DbsField region_Code_MasterPast_End_Date;

    public Map<String, Integer> isnDictionary() { return isnDictionary; }
    public void isnDictionary(Map<String, Integer> isnDictionary) { this.isnDictionary = isnDictionary; }

    public DbsGroup getRegion_Code_MasterPeGroups() { return region_Code_MasterPeGroups; }


    public DbsGroup getRegion_Code_MasterPast_City_Pair_Info() { return region_Code_MasterPast_City_Pair_Info; }


    public DbsGroup getRegion_Code_MasterPast_City_Pair_InfoGroup() { return region_Code_MasterPast_City_Pair_InfoGroup; }


    public DbsField getRegion_Code_MasterPast_City_Pair_InfoCast() { return region_Code_MasterPast_City_Pair_InfoCast; }


    public DbsGroup getRegion_Code_MasterPast_City_Pair_InfoRedef() { return region_Code_MasterPast_City_Pair_InfoRedef; }


    public DbsField getRegion_Code_MasterPast_City_Pair_InfoBinary() { return region_Code_MasterPast_City_Pair_InfoBinary; }


    public DbsField getRegion_Code_MasterPast_Segment_Miles() { return region_Code_MasterPast_Segment_Miles; }


    public DbsField getRegion_Code_MasterPast_Region_Code_1() { return region_Code_MasterPast_Region_Code_1; }


    public DbsField getRegion_Code_MasterPast_Dot_Miles() { return region_Code_MasterPast_Dot_Miles; }


    public DbsField getRegion_Code_MasterPast_Status_Indicator() { return region_Code_MasterPast_Status_Indicator; }


    public DbsField getRegion_Code_MasterPast_Start_Date() { return region_Code_MasterPast_Start_Date; }


    public DbsField getRegion_Code_MasterPast_End_Date() { return region_Code_MasterPast_End_Date; }


    //Initialization Methods
    private void initializeFields() throws Exception
    {
        peFields = new DbsRecord();
        isnDictionary = new HashMap<String, Integer>();

        initializeFields_1();

    }

    

private void initializeFields_1() throws Exception
{

        region_Code_MasterPeGroups = peFields.newGroupInRecord("region_Code_MasterPeGroups", "REGION-CODE-MASTERPeGroups");

        // REGION-CODE-MASTER_PAST_CITY_PAIR_INFO
        region_Code_MasterPast_City_Pair_InfoGroup = region_Code_MasterPeGroups.newGroupInGroup("region_Code_MasterPast_City_Pair_InfoGroup", "region_Code_MasterPast_City_Pair_InfoGroup");
        region_Code_MasterPast_City_Pair_InfoCast = region_Code_MasterPast_City_Pair_InfoGroup.newFieldInGroup("region_Code_MasterPast_City_Pair_InfoCast", "C*region_Code_MasterPast_City_Pair_Info", FieldType.NUMERIC, 3);
        region_Code_MasterPast_City_Pair_Info = region_Code_MasterPast_City_Pair_InfoGroup.newGroupArrayInGroup("region_Code_MasterPast_City_Pair_Info", "PAST-CITY-PAIR-INFO", new DbsArrayController(1, 191), RepeatingFieldStrategy.PeriodicGroupFieldArray, "REGION_CODE_MST_PAST_CITY_PAIR_INFO");
        region_Code_MasterPast_Segment_Miles = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_Segment_Miles", "PAST-SEGMENT-MILES", FieldType.NUMERIC, 4, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_SEGMENT_MILES");
        region_Code_MasterPast_Region_Code_1 = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_Region_Code_1", "PAST-REGION-CODE-1", FieldType.STRING, 2, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_REGION_CODE_1");
        region_Code_MasterPast_Dot_Miles = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_Dot_Miles", "PAST-DOT-MILES", FieldType.NUMERIC, 4, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_DOT_MILES");
        region_Code_MasterPast_Status_Indicator = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_Status_Indicator", "PAST-STATUS-INDICATOR", FieldType.STRING, 1, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_STATUS_INDICATOR");
        region_Code_MasterPast_Start_Date = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_Start_Date", "PAST-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_START_DATE");
        region_Code_MasterPast_End_Date = region_Code_MasterPast_City_Pair_Info.newFieldInGroup("region_Code_MasterPast_End_Date", "PAST-END-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PAST_END_DATE");
        isnDictionary.put("region_Code_MasterPast_City_Pair_Info", 0);
        region_Code_MasterPast_City_Pair_InfoRedef = region_Code_MasterPeGroups.newGroupInGroup("region_Code_MasterPast_City_Pair_InfoRedef", "region_Code_MasterPast_City_Pair_Info_Redef", region_Code_MasterPast_City_Pair_InfoGroup);
        region_Code_MasterPast_City_Pair_InfoBinary = region_Code_MasterPast_City_Pair_InfoRedef.newFieldInGroup("region_Code_MasterPast_City_Pair_InfoBinary", "region_Code_MasterPast_City_Pair_InfoBinary", FieldType.BINARY, 5160);
}

    private void initializeValues() throws Exception
    {
        peFields.reset();
    }

    //Instance Property
    private static ThreadLocal<DdmPeriodicGroups> _instance = new ThreadLocal<DdmPeriodicGroups>();
    public synchronized static DdmPeriodicGroups getInstance() throws Exception 
    {
        if (_instance.get() == null) {
            _instance.set(new DdmPeriodicGroups());
        }
        return _instance.get();
    }

    // Constructor
    private DdmPeriodicGroups() throws Exception
    {
        initializeFields();
        initializeValues();
    }

    // Public Methods
    public DbsGroup getGroups(String ddmName) throws Exception {
        DbsBaseField tempGroup;
        tempGroup = peFields.findDbsBaseFieldByName(ddmName + "PeGroups");
        return (DbsGroup)tempGroup;
    }
}
