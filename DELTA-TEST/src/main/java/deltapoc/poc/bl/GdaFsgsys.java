/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8 - Build 20210115a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-01-15 07:03:38 PM
**        * FROM NATURAL GDA     : FSGSYS
************************************************************
**        * FILE NAME            : GdaFsgsys.java
**        * CLASS NAME           : GdaFsgsys
**        * INSTANCE NAME        : GdaFsgsys
************************************************************ */

package deltapoc.poc.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

public final class GdaFsgsys extends DbsRecord
{
    private static ThreadLocal<List<GdaFsgsys>> _instance = new ThreadLocal<List<GdaFsgsys>>();

    // Properties
    private DbsField pnd_Program;
    private DbsField pnd_Command;
    private DbsField pnd_Command_Prev;
    private DbsField pnd_Date_Closed;
    private DbsField pnd_Personnel_Id;
    private DbsGroup pnd_Date;
    private DbsField pnd_Date_Pnd_Mm;
    private DbsField pnd_Date_Pnd_Dd;
    private DbsField pnd_Date_Pnd_Yy;
    private DbsField pnd_Flight_Number;
    private DbsField pnd_Filler_1;
    private DbsField pnd_Orig;
    private DbsField pnd_Filler_2;
    private DbsField pnd_Dest;
    private DbsField pnd_Region_Code;
    private DbsField pnd_Operation;
    private DbsField pnd_Ship_Number;
    private DbsField pnd_Equipment;
    private DbsField pnd_Request;
    private DbsField pnd_Message_Text;
    private DbsField pnd_Printer_Addr;
    private DbsField pnd_Help_Prompt;
    private DbsField pnd_Help_Prompt_Command;
    private DbsField pnd_Supervisor_Check;
    private DbsField pnd_Print_Module;
    private DbsField pnd_Rje_Data;
    private DbsField pnd_Print_Return;
    private DbsField pnd_Job_Number;
    private DbsGroup pnd_Audit_Workarea;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Request_Code;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Flight_Nbr;
    private DbsGroup pnd_Audit_Workarea_Pnd_Audit_Date;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Day;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Month;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Year;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Station;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Region_Code;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Ship_Number;
    private DbsField pnd_Audit_Workarea_Pnd_Audit_Mark_Field;
    private DbsGroup pnd_Reject_Queue_Data;
    private DbsField pnd_Reject_Queue_Data_Pnd_Reject_Queue;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_Flight_Nbr;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_Date;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_Ind;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_Start_Flight;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_End_Flight;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_In_Date;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_G_Worked;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_T_Worked;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_P_Worked;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_C_Worked;
    private DbsField pnd_Reject_Queue_Data_Pnd_Rq_O_Worked;
    private DbsGroup pnd_Attempt_Queue_Data;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Attempt_Queue;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_Flight_Number;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_Date;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_Ind;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_Start_Flight;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_End_Flight;
    private DbsField pnd_Attempt_Queue_Data_Pnd_Aq_In_Date;
    private DbsField pnd_G_Flight_Type;
    private DbsField pnd_G_Oacds_Screen;
    private DbsField pnd_G_Partner;
    private DbsField pnd_Airline_Code;
    private DbsField pnd_Cc_Airline_Name;

    public DbsField getPnd_Program() { return pnd_Program; }

    public DbsField getPnd_Command() { return pnd_Command; }

    public DbsField getPnd_Command_Prev() { return pnd_Command_Prev; }

    public DbsField getPnd_Date_Closed() { return pnd_Date_Closed; }

    public DbsField getPnd_Personnel_Id() { return pnd_Personnel_Id; }

    public DbsGroup getPnd_Date() { return pnd_Date; }

    public DbsField getPnd_Date_Pnd_Mm() { return pnd_Date_Pnd_Mm; }

    public DbsField getPnd_Date_Pnd_Dd() { return pnd_Date_Pnd_Dd; }

    public DbsField getPnd_Date_Pnd_Yy() { return pnd_Date_Pnd_Yy; }

    public DbsField getPnd_Flight_Number() { return pnd_Flight_Number; }

    public DbsField getPnd_Filler_1() { return pnd_Filler_1; }

    public DbsField getPnd_Orig() { return pnd_Orig; }

    public DbsField getPnd_Filler_2() { return pnd_Filler_2; }

    public DbsField getPnd_Dest() { return pnd_Dest; }

    public DbsField getPnd_Region_Code() { return pnd_Region_Code; }

    public DbsField getPnd_Operation() { return pnd_Operation; }

    public DbsField getPnd_Ship_Number() { return pnd_Ship_Number; }

    public DbsField getPnd_Equipment() { return pnd_Equipment; }

    public DbsField getPnd_Request() { return pnd_Request; }

    public DbsField getPnd_Message_Text() { return pnd_Message_Text; }

    public DbsField getPnd_Printer_Addr() { return pnd_Printer_Addr; }

    public DbsField getPnd_Help_Prompt() { return pnd_Help_Prompt; }

    public DbsField getPnd_Help_Prompt_Command() { return pnd_Help_Prompt_Command; }

    public DbsField getPnd_Supervisor_Check() { return pnd_Supervisor_Check; }

    public DbsField getPnd_Print_Module() { return pnd_Print_Module; }

    public DbsField getPnd_Rje_Data() { return pnd_Rje_Data; }

    public DbsField getPnd_Print_Return() { return pnd_Print_Return; }

    public DbsField getPnd_Job_Number() { return pnd_Job_Number; }

    public DbsGroup getPnd_Audit_Workarea() { return pnd_Audit_Workarea; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Request_Code() { return pnd_Audit_Workarea_Pnd_Audit_Request_Code; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Flight_Nbr() { return pnd_Audit_Workarea_Pnd_Audit_Flight_Nbr; }

    public DbsGroup getPnd_Audit_Workarea_Pnd_Audit_Date() { return pnd_Audit_Workarea_Pnd_Audit_Date; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Day() { return pnd_Audit_Workarea_Pnd_Audit_Day; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Month() { return pnd_Audit_Workarea_Pnd_Audit_Month; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Year() { return pnd_Audit_Workarea_Pnd_Audit_Year; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Station() { return pnd_Audit_Workarea_Pnd_Audit_Station; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Region_Code() { return pnd_Audit_Workarea_Pnd_Audit_Region_Code; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Ship_Number() { return pnd_Audit_Workarea_Pnd_Audit_Ship_Number; }

    public DbsField getPnd_Audit_Workarea_Pnd_Audit_Mark_Field() { return pnd_Audit_Workarea_Pnd_Audit_Mark_Field; }

    public DbsGroup getPnd_Reject_Queue_Data() { return pnd_Reject_Queue_Data; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Reject_Queue() { return pnd_Reject_Queue_Data_Pnd_Reject_Queue; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_Flight_Nbr() { return pnd_Reject_Queue_Data_Pnd_Rq_Flight_Nbr; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_Date() { return pnd_Reject_Queue_Data_Pnd_Rq_Date; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_Ind() { return pnd_Reject_Queue_Data_Pnd_Rq_Ind; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_Start_Flight() { return pnd_Reject_Queue_Data_Pnd_Rq_Start_Flight; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_End_Flight() { return pnd_Reject_Queue_Data_Pnd_Rq_End_Flight; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_In_Date() { return pnd_Reject_Queue_Data_Pnd_Rq_In_Date; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_G_Worked() { return pnd_Reject_Queue_Data_Pnd_Rq_G_Worked; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_T_Worked() { return pnd_Reject_Queue_Data_Pnd_Rq_T_Worked; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_P_Worked() { return pnd_Reject_Queue_Data_Pnd_Rq_P_Worked; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_C_Worked() { return pnd_Reject_Queue_Data_Pnd_Rq_C_Worked; }

    public DbsField getPnd_Reject_Queue_Data_Pnd_Rq_O_Worked() { return pnd_Reject_Queue_Data_Pnd_Rq_O_Worked; }

    public DbsGroup getPnd_Attempt_Queue_Data() { return pnd_Attempt_Queue_Data; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Attempt_Queue() { return pnd_Attempt_Queue_Data_Pnd_Attempt_Queue; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_Flight_Number() { return pnd_Attempt_Queue_Data_Pnd_Aq_Flight_Number; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_Date() { return pnd_Attempt_Queue_Data_Pnd_Aq_Date; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_Ind() { return pnd_Attempt_Queue_Data_Pnd_Aq_Ind; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_Start_Flight() { return pnd_Attempt_Queue_Data_Pnd_Aq_Start_Flight; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_End_Flight() { return pnd_Attempt_Queue_Data_Pnd_Aq_End_Flight; }

    public DbsField getPnd_Attempt_Queue_Data_Pnd_Aq_In_Date() { return pnd_Attempt_Queue_Data_Pnd_Aq_In_Date; }

    public DbsField getPnd_G_Flight_Type() { return pnd_G_Flight_Type; }

    public DbsField getPnd_G_Oacds_Screen() { return pnd_G_Oacds_Screen; }

    public DbsField getPnd_G_Partner() { return pnd_G_Partner; }

    public DbsField getPnd_Airline_Code() { return pnd_Airline_Code; }

    public DbsField getPnd_Cc_Airline_Name() { return pnd_Cc_Airline_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Program = newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Command = newFieldInRecord("pnd_Command", "#COMMAND", FieldType.STRING, 5);

        pnd_Command_Prev = newFieldInRecord("pnd_Command_Prev", "#COMMAND-PREV", FieldType.STRING, 5);

        pnd_Date_Closed = newFieldInRecord("pnd_Date_Closed", "#DATE-CLOSED", FieldType.STRING, 6);

        pnd_Personnel_Id = newFieldInRecord("pnd_Personnel_Id", "#PERSONNEL-ID", FieldType.STRING, 8);

        pnd_Date = newGroupInRecord("pnd_Date", "#DATE");
        pnd_Date_Pnd_Mm = pnd_Date.newFieldInGroup("pnd_Date_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Date_Pnd_Dd = pnd_Date.newFieldInGroup("pnd_Date_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Date_Pnd_Yy = pnd_Date.newFieldInGroup("pnd_Date_Pnd_Yy", "#YY", FieldType.STRING, 2);

        pnd_Flight_Number = newFieldInRecord("pnd_Flight_Number", "#FLIGHT-NUMBER", FieldType.STRING, 5);

        pnd_Filler_1 = newFieldInRecord("pnd_Filler_1", "#FILLER-1", FieldType.STRING, 2);

        pnd_Orig = newFieldInRecord("pnd_Orig", "#ORIG", FieldType.STRING, 3);

        pnd_Filler_2 = newFieldInRecord("pnd_Filler_2", "#FILLER-2", FieldType.STRING, 2);

        pnd_Dest = newFieldInRecord("pnd_Dest", "#DEST", FieldType.STRING, 3);

        pnd_Region_Code = newFieldInRecord("pnd_Region_Code", "#REGION-CODE", FieldType.STRING, 2);

        pnd_Operation = newFieldInRecord("pnd_Operation", "#OPERATION", FieldType.STRING, 6);

        pnd_Ship_Number = newFieldInRecord("pnd_Ship_Number", "#SHIP-NUMBER", FieldType.STRING, 4);

        pnd_Equipment = newFieldInRecord("pnd_Equipment", "#EQUIPMENT", FieldType.STRING, 9);

        pnd_Request = newFieldInRecord("pnd_Request", "#REQUEST", FieldType.STRING, 6);

        pnd_Message_Text = newFieldInRecord("pnd_Message_Text", "#MESSAGE-TEXT", FieldType.STRING, 71);

        pnd_Printer_Addr = newFieldInRecord("pnd_Printer_Addr", "#PRINTER-ADDR", FieldType.NUMERIC, 4);

        pnd_Help_Prompt = newFieldInRecord("pnd_Help_Prompt", "#HELP-PROMPT", FieldType.STRING, 25);

        pnd_Help_Prompt_Command = newFieldInRecord("pnd_Help_Prompt_Command", "#HELP-PROMPT-COMMAND", FieldType.STRING, 5);

        pnd_Supervisor_Check = newFieldInRecord("pnd_Supervisor_Check", "#SUPERVISOR-CHECK", FieldType.STRING, 1);

        pnd_Print_Module = newFieldInRecord("pnd_Print_Module", "#PRINT-MODULE", FieldType.STRING, 8);

        pnd_Rje_Data = newFieldInRecord("pnd_Rje_Data", "#RJE-DATA", FieldType.STRING, 80);

        pnd_Print_Return = newFieldInRecord("pnd_Print_Return", "#PRINT-RETURN", FieldType.STRING, 5);

        pnd_Job_Number = newFieldInRecord("pnd_Job_Number", "#JOB-NUMBER", FieldType.STRING, 7);

        pnd_Audit_Workarea = newGroupInRecord("pnd_Audit_Workarea", "#AUDIT-WORKAREA");
        pnd_Audit_Workarea_Pnd_Audit_Request_Code = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Request_Code", "#AUDIT-REQUEST-CODE", 
            FieldType.STRING, 1);
        pnd_Audit_Workarea_Pnd_Audit_Flight_Nbr = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Flight_Nbr", "#AUDIT-FLIGHT-NBR", FieldType.STRING, 
            4);
        pnd_Audit_Workarea_Pnd_Audit_Date = pnd_Audit_Workarea.newGroupInGroup("pnd_Audit_Workarea_Pnd_Audit_Date", "#AUDIT-DATE");
        pnd_Audit_Workarea_Pnd_Audit_Day = pnd_Audit_Workarea_Pnd_Audit_Date.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Day", "#AUDIT-DAY", FieldType.STRING, 
            2);
        pnd_Audit_Workarea_Pnd_Audit_Month = pnd_Audit_Workarea_Pnd_Audit_Date.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Month", "#AUDIT-MONTH", FieldType.STRING, 
            2);
        pnd_Audit_Workarea_Pnd_Audit_Year = pnd_Audit_Workarea_Pnd_Audit_Date.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Year", "#AUDIT-YEAR", FieldType.STRING, 
            2);
        pnd_Audit_Workarea_Pnd_Audit_Station = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Station", "#AUDIT-STATION", FieldType.STRING, 
            3);
        pnd_Audit_Workarea_Pnd_Audit_Region_Code = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Region_Code", "#AUDIT-REGION-CODE", 
            FieldType.STRING, 2);
        pnd_Audit_Workarea_Pnd_Audit_Ship_Number = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Ship_Number", "#AUDIT-SHIP-NUMBER", 
            FieldType.STRING, 4);
        pnd_Audit_Workarea_Pnd_Audit_Mark_Field = pnd_Audit_Workarea.newFieldInGroup("pnd_Audit_Workarea_Pnd_Audit_Mark_Field", "#AUDIT-MARK-FIELD", FieldType.NUMERIC, 
            2);

        pnd_Reject_Queue_Data = newGroupInRecord("pnd_Reject_Queue_Data", "#REJECT-QUEUE-DATA");
        pnd_Reject_Queue_Data_Pnd_Reject_Queue = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Reject_Queue", "#REJECT-QUEUE", FieldType.STRING, 
            1);
        pnd_Reject_Queue_Data_Pnd_Rq_Flight_Nbr = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_Flight_Nbr", "#RQ-FLIGHT-NBR", FieldType.STRING, 
            5);
        pnd_Reject_Queue_Data_Pnd_Rq_Date = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_Date", "#RQ-DATE", FieldType.STRING, 6);
        pnd_Reject_Queue_Data_Pnd_Rq_Ind = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_Ind", "#RQ-IND", FieldType.STRING, 1);
        pnd_Reject_Queue_Data_Pnd_Rq_Start_Flight = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_Start_Flight", "#RQ-START-FLIGHT", 
            FieldType.STRING, 5);
        pnd_Reject_Queue_Data_Pnd_Rq_End_Flight = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_End_Flight", "#RQ-END-FLIGHT", FieldType.STRING, 
            5);
        pnd_Reject_Queue_Data_Pnd_Rq_In_Date = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_In_Date", "#RQ-IN-DATE", FieldType.STRING, 
            6);
        pnd_Reject_Queue_Data_Pnd_Rq_G_Worked = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_G_Worked", "#RQ-G-WORKED", FieldType.STRING, 
            1);
        pnd_Reject_Queue_Data_Pnd_Rq_T_Worked = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_T_Worked", "#RQ-T-WORKED", FieldType.STRING, 
            1);
        pnd_Reject_Queue_Data_Pnd_Rq_P_Worked = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_P_Worked", "#RQ-P-WORKED", FieldType.STRING, 
            1);
        pnd_Reject_Queue_Data_Pnd_Rq_C_Worked = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_C_Worked", "#RQ-C-WORKED", FieldType.STRING, 
            1);
        pnd_Reject_Queue_Data_Pnd_Rq_O_Worked = pnd_Reject_Queue_Data.newFieldInGroup("pnd_Reject_Queue_Data_Pnd_Rq_O_Worked", "#RQ-O-WORKED", FieldType.STRING, 
            1);

        pnd_Attempt_Queue_Data = newGroupInRecord("pnd_Attempt_Queue_Data", "#ATTEMPT-QUEUE-DATA");
        pnd_Attempt_Queue_Data_Pnd_Attempt_Queue = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Attempt_Queue", "#ATTEMPT-QUEUE", 
            FieldType.STRING, 1);
        pnd_Attempt_Queue_Data_Pnd_Aq_Flight_Number = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_Flight_Number", "#AQ-FLIGHT-NUMBER", 
            FieldType.STRING, 5);
        pnd_Attempt_Queue_Data_Pnd_Aq_Date = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_Date", "#AQ-DATE", FieldType.STRING, 
            6);
        pnd_Attempt_Queue_Data_Pnd_Aq_Ind = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_Ind", "#AQ-IND", FieldType.STRING, 1);
        pnd_Attempt_Queue_Data_Pnd_Aq_Start_Flight = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_Start_Flight", "#AQ-START-FLIGHT", 
            FieldType.STRING, 5);
        pnd_Attempt_Queue_Data_Pnd_Aq_End_Flight = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_End_Flight", "#AQ-END-FLIGHT", 
            FieldType.STRING, 5);
        pnd_Attempt_Queue_Data_Pnd_Aq_In_Date = pnd_Attempt_Queue_Data.newFieldInGroup("pnd_Attempt_Queue_Data_Pnd_Aq_In_Date", "#AQ-IN-DATE", FieldType.STRING, 
            6);

        pnd_G_Flight_Type = newFieldInRecord("pnd_G_Flight_Type", "#G-FLIGHT-TYPE", FieldType.STRING, 5);

        pnd_G_Oacds_Screen = newFieldInRecord("pnd_G_Oacds_Screen", "#G-OACDS-SCREEN", FieldType.BOOLEAN);

        pnd_G_Partner = newFieldInRecord("pnd_G_Partner", "#G-PARTNER", FieldType.STRING, 2);

        pnd_Airline_Code = newFieldInRecord("pnd_Airline_Code", "#AIRLINE-CODE", FieldType.STRING, 2);

        pnd_Cc_Airline_Name = newFieldInRecord("pnd_Cc_Airline_Name", "#CC-AIRLINE-NAME", FieldType.STRING, 25);

        this.setRecordName("GdaFsgsys");
    }
    public void initializeValues() throws Exception
    {
        reset();

        pnd_G_Partner.setInitialValue("DL");
    }

    // Constructor
    private GdaFsgsys() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaFsgsys getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaFsgsys>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaFsgsys());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

